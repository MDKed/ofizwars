package com.mdked.owars.Onet;

import com.esotericsoftware.kryo.Kryo;
import com.mdked.owars.core.World;
import com.mdked.owars.units.*;

public class KryReg {
	public static void regShit(Kryo kryo) {
		
		kryo.register(Poked.class);
		kryo.register(DataPok.class);
		kryo.register(World.class);
		kryo.register(java.util.ArrayList.class);
		kryo.register(WPok.class);
		kryo.register(WorPok.class);
		kryo.register(EnDPak.class);
		kryo.register(RemOPok.class);
				
		kryo.register(Weapon.class);
		kryo.register(Bullet.class);
		
		kryo.register(Entity.class);
		kryo.register(Moveable.class);
		kryo.register(Fighter.class);
		kryo.register(Player.class);
		kryo.register(Enemy.class);
		kryo.register(Squad.class);
		
		kryo.register(Pregrada.class);
		kryo.register(int[].class);
		kryo.register(int[][].class);
		kryo.register(Flag.class);
		kryo.register(Respawn.class);
		kryo.register(byte[].class);
		kryo.register(byte[][].class);
		
		kryo.register(Musor.class);
		
		
	}
}
