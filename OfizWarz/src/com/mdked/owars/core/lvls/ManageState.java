package com.mdked.owars.core.lvls;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.mdked.owars.core.Maino;
import com.mdked.owars.core.World;
import com.mdked.owars.core.werld.Fraction;

public class ManageState extends BasicGameState {

	private int stateID;
	private World wld;
	private Fraction fraction;
	private int xStep = 100, yStep = 50;
	private int timer1;
	private Animation city;

	public ManageState( int stateID)
	{
		this.stateID = stateID;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		city = new Animation(new SpriteSheet(new Image("img/MapIco.png", false,
				Image.FILTER_NEAREST), 64, 64), 100);

	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		g.setBackground(Color.gray);

		g.setColor(Color.black);
		g.drawString(" Fraction : " + fraction.name, xStep, yStep);
		g.drawString(" Money : " + fraction.money + " Creds", xStep, yStep*2);
		g.drawString(" Soldiers : " + fraction.forces , xStep, yStep*3);




		int sx = xStep*5, sy = gc.getHeight()/10, pex = 2;
		g.fillRect(sx-pex , sy-pex ,  4+wld.mape.mapo.length*pex, 4+wld.mape.mapo[0].length*pex);
		g.setColor(Color.darkGray);
		g.fillRect(sx , sy ,  wld.mape.mapo.length*pex, wld.mape.mapo[0].length*pex);
		g.setClip(sx, sy, wld.mape.mapo.length*pex, wld.mape.mapo[0].length*pex);

		for(int x = 0; x < wld.mape.mapo.length; x++)
			for(int y = 0; y < wld.mape.mapo[0].length; y++) {
				if (wld.mape.mapo[x][y] != null) 
					//					g.setColor(wld.mape.mapo[x][y]);
//					System.out.println(wld.mape.mapo[x][y].toString());
					switch (wld.mape.mapo[x][y].toString()) {
					case "Color (1.0,0.0,0.0,1.0)":
						g.setColor(Color.green);
						g.fillRect(sx + x*pex-5, sy+ y*pex-5, 10f, 10f);
						break;
						
					case "Color (0.0,1.0,0.1,1.0)":
						city.getImage(1).draw(sx + x*pex-32, sy+ y*pex-32, Color.green);
						break;
						
					case "Color (0.0,1.0,0.2,1.0)":
						city.getImage(1).draw(sx + x*pex-32, sy+ y*pex-32, Color.red);
						break;
						
					case "Color (0.0,1.0,0.3,1.0)":
						city.getImage(1).draw(sx + x*pex-32, sy+ y*pex-32, Color.yellow);
						break;
						
					case "Color (0.0,0.1,1.0,1.0)":
						city.getImage(0).draw(sx + x*pex-32, sy+ y*pex-32, Color.green);
						break;
					}
				
			}
		g.clearClip();
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		timer1 += delta;
		if (Maino.started && wld == null ) { 
			wld = Maino.wld;
			fraction = wld.getPlrFr();	
		}
		//		if (timer1 > 3000) {
		wld.mape.refresh( );
		//			timer1 = 0;
		//		}

		Input input = gc.getInput();

		if (input.isKeyPressed(Input.KEY_ESCAPE) || input.isKeyPressed(Input.KEY_N)) {
			game.enterState(Main.GAMEPLAYSTATE);
		}

	}

	@Override
	public int getID() {
		return stateID;
	}

}
