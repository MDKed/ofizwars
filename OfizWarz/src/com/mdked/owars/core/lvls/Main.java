package com.mdked.owars.core.lvls;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import com.mdked.owars.core.Maino;

public class Main extends StateBasedGame  {

	public static final int MAINMENUSTATE  =  0;
	public static final int GAMEPLAYSTATE          = 1;
	public static final int MANAGERSTATE  =  2;
	
	public Main() {
		super("OWars");
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		try {
			AppGameContainer appgc;
			appgc = new AppGameContainer(new Main());
			appgc.setDisplayMode(1024, 768, false);
			//			appgc.setDisplayMode(1920, 1080, true);

			appgc.setMinimumLogicUpdateInterval(15);
			//			appgc.setMouseGrabbed(true);
			appgc.setTargetFrameRate(100);
			appgc.setAlwaysRender(true);

			appgc.start();
		} catch (SlickException ex) {
			Logger.getLogger(Maino.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	public void initStatesList(GameContainer container) throws SlickException {

		this.addState(new MainMenu(MAINMENUSTATE));
		
		this.addState(new Maino(GAMEPLAYSTATE));
		
		this.addState(new ManageState(MANAGERSTATE));
		
	}

}
