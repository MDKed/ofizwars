package com.mdked.owars.core.lvls;

import java.io.File;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MainMenu extends BasicGameState  {
	
	private int stateID;
	private int startInt;

	private boolean drawSaves;
	private static boolean loading;

	private static String[] savesList;
	private static int loadNum;
	
	public MainMenu( int stateID)
	{
		this.stateID = stateID;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		File checksaves = new File("saves/");
		savesList =
				checksaves.list();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		drawStartMenu(gc,g);
		
	}
	private void drawStartMenu(GameContainer gc, Graphics g) {
		int wi = gc.getWidth(), he = gc.getHeight();
		if (!loading) {
			g.drawString("1 - Start new Game", wi/10, he/10);

			if (drawSaves)
				g.drawString("2 - Load Game", wi/10, he*2/10);
		} else {
			for (int a = 0; a < savesList.length; a++) 
				g.drawString(a+1 + " - " + savesList[a], wi/10, he*(a+1)/10);
		}

		g.setColor(Color.blue);
		g.setLineWidth(4);
		if (!loading && drawSaves && startInt < 3 || !drawSaves && startInt < 2 || loading && startInt <= savesList.length)
		g.drawRect(wi/10 -10, (he/10 - 5)*startInt, 200, 30);
		g.setColor(Color.white);

	}
	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		//TODO STARTMENU
		Input input = gc.getInput();
		
		int he = gc.getHeight();

		Input inp = gc.getInput();
		if (inp.getMouseY() > he/10 ) 
			startInt = (inp.getMouseY()+15)/(he/10);
		//			else startInt = 0;&& inp.getMouseY() < he/10+20

		if (savesList != null && savesList.length > 0)
			drawSaves = true;

		if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
			if (!loading)
				switch (startInt) {
				case 1:
					game.enterState(Main.GAMEPLAYSTATE);
					break;
				case 2:
					loadGameState();
					break;
				}
			else if (startInt-1 <= savesList.length)
				loadGame(startInt-1, game);
		}
		if (input.isKeyPressed(Input.KEY_1))
			if (!loading)
				game.enterState(Main.GAMEPLAYSTATE);
			else 
				loadGame(0, game);

		if (savesList != null)
			if (input.isKeyPressed(Input.KEY_2))

				if (!loading)
					loadGameState();
				else 
					loadGame(1, game);
		
		if (input.isKeyPressed(Input.KEY_ESCAPE) && loading)
			loading = false;
			
		
	}

	

	private void loadGameState() {
		if (drawSaves)
			loading = true;
	}
	private void loadGame(int numer, StateBasedGame game) {
		loadNum = numer;
		game.enterState(Main.GAMEPLAYSTATE);
	}
	
	@Override
	public int getID() {
		return stateID;
	}

	public static boolean isLoading() {
		// TODO Auto-generated method stub
		return loading;
	}

	public static String[] getList() {
		return savesList;
	}

	public static int getLNum() {
		return loadNum;
	}

}
