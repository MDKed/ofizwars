package com.mdked.owars.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;




public class FileManager {

	public static void SaveObject(Object obj) {
		File where = new File("");

			where = new File( "saves/" + Maino.wld.seed + "/game.dat");

		try
		{
			FileOutputStream fileOut =
					new FileOutputStream(where);
			ObjectOutputStream out =
					new ObjectOutputStream(fileOut);
			out.writeObject(obj);
			out.close();
			fileOut.close();
		}catch(IOException i)
		{
			System.out.println(" SaveObject error " + where);
			i.printStackTrace();
		}
	}


	/**
	 * 1 - map
	 * 2 - oChunk
	 * @param xCh
	 * @param yCh
	 * @param type
	 * @return Object
	 */
	//, int type
	public static Object loadObject(int xCh, int yCh) {

		Object returno = null;
		File objToLoad = new File("");

			objToLoad = new File( Maino.wld.seed + "/map/"+ xCh +"x"+ yCh + ".map");
		
		try
		{

			FileInputStream fileIn =
					new FileInputStream(objToLoad);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			returno =  in.readObject();
			in.close();
			fileIn.close();
		}catch(IOException i)
		{
			i.printStackTrace();

		}catch(ClassNotFoundException c)
		{
			c.printStackTrace();
		}
		
//		Maino.wld.chunks.add(returno);
		
		return returno;


	}


	public static World loadObject(String string) {
		Object returno = null;
		File objToLoad = new File("");

			objToLoad = new File( "saves/" + string + "/game.dat");
		
		try
		{

			FileInputStream fileIn =
					new FileInputStream(objToLoad);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			returno =  in.readObject();
			in.close();
			fileIn.close();
		}catch(IOException i)
		{
			i.printStackTrace();

		}catch(ClassNotFoundException c)
		{
			c.printStackTrace();
		}
		
		
		return (World) returno;


	}
}
