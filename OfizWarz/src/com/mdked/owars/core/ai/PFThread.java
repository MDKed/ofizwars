package com.mdked.owars.core.ai;

import com.mdked.owars.core.Maino;
import com.mdked.owars.units.APath;
import com.mdked.owars.units.Enemy;

import grid.GridLocation;

public class PFThread implements Runnable{
	private Thread t;

	private Enemy starter;
	private float xe, ye;

	public PFThread(Enemy zakaz, float x, float y) {
		starter = zakaz;
		xe = x;
		ye = y;
	}
	
	@Override
	public void run() {
//		System.out.println("starting pf");
		//		long tiem = System.currentTimeMillis();
//		Maino.getDelta();
		APath rpth = null;
		if (!Maino.aim.asmapI) 
			Maino.aim.makeASMap();

		float[] strt = Maino.aim.getOtnXY(starter.x+starter.xxWidth, starter.y+starter.yyWidth), endd = Maino.aim.getOtnXY(xe+starter.xxWidth, ye+starter.yyWidth);


		//		if (!end)
		//		System.out.println( "\n" + starter.id + " -start pf " + (int) (strt[0]/16) + "x"+ (int) (strt[1]/16) );dsa
		GridLocation start = 
				//				new GridLocation((int) xs/16,(int) ys/16, false);
				Maino.aim.getNotWalledPoint(strt[0], strt[1], false);
		//					 new GridLocation((int) tgtf.x/16,(int)  tgtf.y/16, true);
		GridLocation end = Maino.aim.getNotWalledPoint(endd[0], endd[1], true);


		//		long tiem2 = System.currentTimeMillis();
		//		System.out.println("notwaled pf " + (tiem2 - tiem) + " xyS " + start.getX() + "x" + start.getY() + " ende " + end.getX() + "x" + end.getY());
		//		tiem = System.currentTimeMillis();

		if (start.getX() != end.getX() && start.getY() != end.getY()) 
			rpth = new APath(Maino.aim.pathfinding.getPath(start, end , Maino.aim.asMap ), (int) Maino.aim.otnX, (int) Maino.aim.otnY);
		//			rpth.path = pathfinding.getPath(start, end , asMap );
		//s			rpth.otnX = (int) otnX;
		//			rpth.otnY = (int) otnY;

		//		tiem2 = System.currentTimeMillis();
		//		System.out.println("pf pf " + (tiem2 - tiem));
		//		Maino.rpth = rpth;
		starter.givePath(rpth);
//		Maino.getDelta();
//		return rpth;
	}
	public void start ()
	{
//		System.out.println("Starting PF for " + faza );
		if (t == null)
		{
			t = new Thread (this);
			t.setPriority(5);
			t.start ();
		}
	}
}
