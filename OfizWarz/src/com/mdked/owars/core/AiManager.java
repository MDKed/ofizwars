package com.mdked.owars.core;

import grid.GridLocation;
import grid.GridMap;
import grid.GridPathfinding;

import java.util.ArrayList;

import javax.annotation.Generated;

import com.mdked.owars.core.ai.PFThread;
import com.mdked.owars.core.werld.Fraction;
import com.mdked.owars.units.APath;
import com.mdked.owars.units.Enemy;
import com.mdked.owars.units.Entity;
import com.mdked.owars.units.Flag;
import com.mdked.owars.units.Player;
import com.mdked.owars.units.Pregrada;
import com.mdked.owars.units.Respawn;
import com.mdked.owars.units.strat.Facility;

import core.Path;
//implements Runnable
public class AiManager  {

	//	private Thread t;

	private World wld;

	//	public ArrayList<Enemy> ais;
	private int  timer1, timer2;
	public GridMap asMap;
	public float otnX, otnY;
	private boolean clearing;

	public boolean asmapI;

	public GridPathfinding pathfinding;

	private boolean gamesadyes;

	private int delta;

	private int deltap;


	public AiManager(World wld) {
		this.wld = wld;
		//		ais = new ArrayList<Enemy>();
		asMap = new GridMap(wld.worldbsize*2, wld.worldbsize*2);
		//		tf1 = 0;
		//		tf2 = 0;

		timer1 = 0;
		timer2 = 0;

		pathfinding = new GridPathfinding();
		//		initASMap();
	}

	//	public synchronized void go(int delta) {
	//		//		gamesadyes = true;
	//		deltap += delta;
	//		synchronized (t) {
	//			t.notify();
	//		}
	//	}

	//	public void start ()
	//	{
	//		//		System.out.println("Starting PF for " + faza );
	//		if (t == null)
	//		{
	//			t = new Thread (this, "AiManager");
	//			t.setPriority(6);
	//
	//			t.start();
	//
	//		}
	//	}

	public void makeASMap() {
		asmapI = true;
		//		System.out.println(" making asMap" );
		asMap = new GridMap(wld.worldbsize*2, wld.worldbsize*2);
		ArrayList<Entity> objcs = wld.getObjects();

		otnX = (int) (wld.plr.x/World.kvx - wld.worldbsize)*World.kvx ;
		otnY =  (int) (wld.plr.y/World.kvx - wld.worldbsize)*World.kvx ;
		//						System.out.println(" -==============- otnXy" + otnX + "x" + otnY );

		//		int startxy = asker.x/16 -


		for (int a = 0;a < objcs.size(); a++ ) {

			//			if (World.getDist(asker, objcs.get(a)) > 1024) 
			//				continue;


			Entity ent = objcs.get(a);
			if (ent == null) continue;
			int xx = (int) ((ent.x)/World.kvx);//+1
			if ((wld.plr.x/World.kvx - wld.worldbsize)*World.kvx >= 0) ++xx;
			int yy = (int) ((ent.y)/World.kvx);
			if ((wld.plr.y/World.kvx - wld.worldbsize)*World.kvx >= 0) ++yy;




			if (xx <  wld.plr.x/World.kvx - wld.worldbsize && xx >  wld.plr.x/World.kvx + wld.worldbsize) {
				System.out.println(" ZAZA asmap xx " + xx +"x" + yy);
				continue;
			}
			if (yy <  wld.plr.y/World.kvx - wld.worldbsize && yy >  wld.plr.y/World.kvx + wld.worldbsize) {
				System.out.println(" ZAZAZasmap xx " + xx +"x" + yy);
				continue;
			}

			//			xx += 64;
			//			yy += 64;

			int[] wtd =	{(int) (xx -( wld.plr.x/World.kvx) + wld.worldbsize), (int) (yy - ( wld.plr.y/World.kvx) + wld.worldbsize)};

			int x = wtd[0];
			int y = wtd[1];
			if (x < 0 || x >= asMap.getSizeX()) continue;
			if (y < 0 || y >= asMap.getSizeY()) continue;

			//			Entity ent = wld.objcs.get(a);
			//			if (ent.y < 0) continue;
			//			System.out.println(" asmap " + x +"x" + y);

			switch (objcs.get(a).type) {
			case 3:
				asMap.set(x, y, GridMap.WALL);
				continue;


			case 7:
				asMap.set(x, y, GridMap.WALL);
				if (y+1 < asMap.getSizeY())
					asMap.set(x, y+1, GridMap.WALL);
				continue;
			case 8:
				Pregrada preg = (Pregrada) objcs.get(a);
				if (preg.ctype >=100) {
					drawWalls(x, y, 5, 5);
					continue;
				}
			}
		}
	}

	private void drawWalls(int x, int y, int width, int height) {
		for (int xx = 0; xx < width; xx++) 
			for(int yy = 0; yy < height; yy++) 
				if (x+xx < asMap.getSizeX() && y+yy < asMap.getSizeY())
					asMap.set(x+xx, y+yy, GridMap.WALL);
	}

	public void addSqdMate(Player sqdldr, Respawn spn) {
		addSqdMate(sqdldr, spn.x, spn.y);

	}
	public void addSqdMate(Player sqdldr, float sX, float sY) {
		Enemy newbot = new Enemy((int)sX, (int) sY, sqdldr.team);
		wld.addObj(newbot,  true, false, false);
		newbot.giveWeapon((int) (Math.random()*2));
		addBot(newbot);

		//		newbot.moveTo(getPathto(spn.x, spn.y, sqdldr.x, sqdldr.y));
		newbot.followTarget(sqdldr.addSqdMt(newbot));

	}

	private void addBot(Enemy boto) {
		wld.ais.add(boto);
//		System.out.println(" new bot ");
	}
	
	public void addBot(Fraction team, Respawn spawn, Facility fac) {
		int[] xy = {(int) spawn.x, (int) spawn.y};
		addBot(team, xy,fac) ;
	}
	
	public void addBot(Fraction team, Respawn spawn) {
		int[] xy = {(int) spawn.x, (int) spawn.y};
		addBot(team, xy);
	}
	
	public void addBot(Fraction team, int[] spawn, Facility fac) {
		Enemy newbot = new Enemy(spawn[0], spawn[1], team);

		wld.addObj(newbot, true, false, false);
		newbot.giveWeapon((int) (Math.random()*2));
		newbot.facTarget = fac;
		
		addBot(newbot);
	}
	
	public void addBot(Fraction team, int[] spawn) {
		
		Enemy newbot = new Enemy(spawn[0], spawn[1], team);

		wld.addObj(newbot, true, false, false);
		newbot.giveWeapon((int) (Math.random()*2));
		
		addBot(newbot);

	}

	//	private int getline(int team) {
	//		int[] chl = team == 1 ? lines1 : lines2;
	//
	//		for (int a = 0; a < chl.length; a++) 
	//			if (chl[a] < wld.botQ) {
	//				chl[a]++;
	//				return a;
	//			}
	//
	//		return -1;
	//
	//
	//	}

	//	public void getFlagTarget(Enemy bot, int line) {
	//		int kek = 0;
	//		ArrayList<Entity> objcs = wld.getObjects();
	//		for (int a = 0; a < objcs.size();a++ )
	//			if ( objcs.get(a).type == 4 ) {
	//				Flag tgtf = (Flag) objcs.get(a);
	//				//				int chui = bot.team == 1 ? tf1 : tf2;
	//				//				System.out.println("kek = " + kek + " chui = " + chui);
	//
	//				if (kek == line) {
	//					//					GridLocation start = new GridLocation((int) bot.x/16,(int)   bot.y/16, false);
	//					//					//					 new GridLocation((int) tgtf.x/16,(int)  tgtf.y/16, true);
	//					//					GridLocation end = getNotWalledPoint(tgtf.x, tgtf.y, true);
	//					//
	//					//					GridPathfinding pathfinding = new GridPathfinding();
	//					//
	//					//					Path pth = pathfinding.getPath(start, end , asMap );
	//
	//					bot.moveTo(getPathto( bot, (int) tgtf.x,(int)  tgtf.y));
	//					bot.line = line;
	//					//					System.out.println("move bot! " + bot.path);
	//					//					if (bot.team == 1) tf1++;
	//					//					else
	//					//						tf2++;
	//					break;
	//
	//				}
	//				kek++;
	//			}
	//		//		System.out.println("kek = " + kek );
	//
	//		//		if (tf1 >= 3) tf1 = 0;
	//		//		if (tf2 >= 3) tf2 = 0;
	//	}

	public float[] getOtnXY(float x, float y) {
		float[] ret = {	x - otnX, y - otnY };
		return ret;
	}

	//TODO make threaded
	public void getPathto(Enemy starter, float xe, float ye) {
		PFThread pftr = new PFThread(starter, xe, ye);
		pftr.start();
		
//		//		System.out.println("starting pf");
//		//		long tiem = System.currentTimeMillis();
//		Maino.getDelta();
//		APath rpth = null;
//		if (!asmapI) 
//			makeASMap();
//
//		float[] strt = getOtnXY(starter.x+16, starter.y+16), endd = getOtnXY(xe+16, ye+16);
//
//
//		//		if (!end)
//		//		System.out.println( "\n" + starter.id + " -start pf " + (int) (strt[0]/16) + "x"+ (int) (strt[1]/16) );
//		GridLocation start = 
//				//				new GridLocation((int) xs/16,(int) ys/16, false);
//				getNotWalledPoint(strt[0], strt[1], false);
//		//					 new GridLocation((int) tgtf.x/16,(int)  tgtf.y/16, true);
//		GridLocation end = getNotWalledPoint(endd[0], endd[1], true);
//
//
//		//		long tiem2 = System.currentTimeMillis();
//		//		System.out.println("notwaled pf " + (tiem2 - tiem) + " xyS " + start.getX() + "x" + start.getY() + " ende " + end.getX() + "x" + end.getY());
//		//		tiem = System.currentTimeMillis();
//
//		if (start.getX() != end.getX() && start.getY() != end.getY()) 
//			rpth = new APath(pathfinding.getPath(start, end , asMap ), (int) otnX, (int) otnY);
//		//			rpth.path = pathfinding.getPath(start, end , asMap );
//		//s			rpth.otnX = (int) otnX;
//		//			rpth.otnY = (int) otnY;
//
//		//		tiem2 = System.currentTimeMillis();
//		//		System.out.println("pf pf " + (tiem2 - tiem));
//		//		Maino.rpth = rpth;
//		Maino.getDelta();
//		return rpth;
	}

	public GridLocation getNotWalledPoint(float x, float y, boolean end) {
		//FIXME SHIUIIIIIIT
		int xx = (int) (x/World.kvx), yy = (int) (y/World.kvx),
				iy = (int) (y/World.kvx), ix = (int) (x/World.kvx);
		//		if (!end)
		//		System.out.println(" gnwp start " + xx +"x" + yy);
		if (x%16 < 8) xx++;
		if (y%16 < 8) yy++;

		if (xx <= 0) xx = 1;
		if (yy <= 0) yy = 1;
		if (xx >= asMap.getSizeX()-5) xx = asMap.getSizeX()-5;
		if (yy >= asMap.getSizeY()-5) yy = asMap.getSizeY()-5;

		//		boolean upclose;


		if (asMap.get(xx, yy) == GridMap.WALL) {
			if (yy > 1)
				yy--;

			//			if (!end)
			//				System.out.println("  gnwp step1 " + xx +"x" + yy);

			if (asMap.get(xx, yy) != GridMap.WALL)
				return new GridLocation(xx, yy, end);

			//			else upclose = true;


			if (yy < wld.worldbsize-5 ) {
				//				if (yy == 1) 
				//					yy++;
				yy += 2;
			}
			if (yy >= asMap.getSizeY()) yy = asMap.getSizeY()-1;
			//			if (!end)
			//			System.out.println("   gnwp step2 " + xx +"x" + yy);
			if (asMap.get(xx, yy) != GridMap.WALL)
				return new GridLocation(xx, yy, end);


			yy = iy;
			if (iy < 0) yy = 0;
			if (iy >= asMap.getSizeY()) yy = asMap.getSizeY()-1;
			if (xx > 1)
				xx--;

			//			if (!end)
			//			System.out.println("    gnwp step3 " + xx +"x" + yy);

			if (asMap.get(xx, yy) != GridMap.WALL)
				return new GridLocation(xx, yy, end);

			if (xx < wld.worldbsize-5 ) {
				//				if (xx == 1) 
				//					xx++;
				xx += 2;
			}
			if (xx >= asMap.getSizeX()) xx = asMap.getSizeX()-1;

			//			if (!end)
			//			System.out.println("     gnwp step4 " + xx +"x" + yy);

			if (asMap.get(xx, yy) != GridMap.WALL)
				return new GridLocation(xx, yy, end);

			//			xx = ix;
			//			if (upclose) {
			//				if (yy < wld.worldbsize-5 ) 
			//					yy++;
			//				if (xx > 1)
			//					xx--;
			//				if (xx < wld.worldbsize-5 ) 
			//					xx += 2;
			//				if (asMap.get(xx, yy) != GridMap.WALL)
			//					return new GridLocation(xx, yy, end);
			//			} else {
			//				if (yy > 1)
			//					yy--;
			//				if (xx > 1)
			//					xx--;
			//				if (asMap.get(xx, yy) != GridMap.WALL)
			//					return new GridLocation(xx, yy, end);
			//				if (xx < wld.worldbsize-5 ) 
			//					xx += 2;
			//				if (asMap.get(xx, yy) != GridMap.WALL)
			//					return new GridLocation(xx, yy, end);
			//			}
			//			//old
			//			int xit = 0;
			//			//			System.out.println(" xxyy " + xx + "x" + yy);
			//
			//			while (yy < wld.worldbsize && xx < wld.worldbsize && asMap.get(xx, yy) == GridMap.WALL && xit < 3)
			//			{
			//				xx++;
			//				xit++;
			//				int yit = 0;
			//				while (yy < wld.worldbsize && asMap.get(xx, yy) == GridMap.WALL&& yit < 3) {
			//					yy++;
			//					yit++;
			//				}
			//			};
		}

		//		System.out.println(" tgt xy " + xx + "x" + yy);
		return new GridLocation(xx, yy, end);

	}

	public void killBot(int id) {
		Enemy ftop = (Enemy) wld.getIdO(id);

		if (ftop.facTarget != null) 
			ftop.facTarget.soldierLost();
	}
	
	public void remove(int id) {
		
		if (clearing) return;
		
		Enemy ftop = (Enemy) wld.getIdO(id);

//		if (ftop.facTarget != null) 
//			ftop.facTarget.soldierLost();
		//		ArrayList<Enemy> chke = ftop.team == 1 ? team1 : team2;
		//		int[] lino = ftop.team == 1 ? lines1 : lines2;

		for (int a = 0; a < wld.ais.size(); a++) 
			if (wld.ais.get(a).id == id) {

				if (ftop.follow) 
					ftop.sqd.remFighter(ftop);

				wld.ais.remove(a);
				return;
			}

		//		else {

		//		}

		//		for (int b = 0; b < team2.size(); b++) 
		//			if (team2.get(b).id == id) {
		//				team2.remove(b);
		//				return;
		//			}
	}

//	public void remAifrArr(Enemy botik) {
//		wld.ais.remove(botik);
//	}
	
	public void clearo() {
		clearing = true;
		//		for (int x = 0; x < 2; x++) {
		//			ArrayList<Enemy> chke = x == 0 ? team1 : team2;
		//			int[] lino = x== 0 ? lines1 : lines2;
		for (int a = 0; a < wld.ais.size(); a++) {
			//				System.out.println(" clearo " + a);
			//				lino[chke.get(a).line]--;
			wld.remObj(wld.ais.get(a));
			//				System.out.println(" removed " + chke.remove(a) + " arr size "+ chke.size());

			//				chke.remove(a);
		}
		wld.ais.clear();

		//		}
		//		int sizl = lines1.length;
		//		//		System.out.println(" t1s " + team1.size() +
		//		//				" t2s " + team2.size());
		//
		//
		//		lines1 = new int[sizl];
		//		lines2 = new int[sizl];
		//		for (int a = 0; a < wld.objcs.size(); a++ ) 
		//			if (wld.objcs.get(a).type == 1 ) {
		//				remove(wld.objcs.get(a).id);
		//				wld.objcs.remove(a);
		//			}
		clearing = false;

		wld.plr.sqd.roster.clear();
	}

	public void manage(int delta) {
//						Maino.getDelta();
		timer1 += delta;
		//		System.out.println(" MANAGE " + delta);
		if (timer1 >= 300) {
			timer1 = 0;
			Respawn cspn = wld.getSpawn(wld.plr.team, wld.plr);
			if ( cspn != null 
					&& World.getDist(wld.plr, cspn) < 100 
					&& wld.plr.sqd.roster.size() < 15) {

//				if (cspn.origFac.type == 201) {
					cspn.origFac.plrSqdSpwn();
//				}

				addSqdMate(wld.plr, cspn);

			}


			for (int a = 0; a < wld.cobj.size(); a++) {
				if (wld.cobj.get(a).type < 100)
					continue;
				Facility fac = (Facility) wld.cobj.get(a);
				if (fac.built && World.getDist(fac, wld.plr) < 6400 ) 
					fac.spawnSoldier();


				//				System.out.println(" dist to fac = " + World.getDist(wld.cobj.get(a), wld.plr));

			}
			//			if (team1.size() < wld.botQ)
			//					addBot(1);

			//			if (team2.size() < wld.botQ)
			//					addBot(2);
			//		
		}
//		Maino.getDelta(false);
//		System.out.println(" AI SIZE = " + wld.ais.size() );
		for (int a = 0; a < wld.ais.size(); a ++) {
//			System.out.println(" AI SIZE = " + wld.ais.size() );
			//			try {
			//				Thread.sleep(1);
			//			} catch (InterruptedException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			}
			if (World.getDist(wld.plr, wld.ais.get(a)) < 4000) {

				//				System.out.println(" indist = " + World.getDist(wld.plr, wld.ais.get(a)));
				wld.ais.get(a).think(delta);
			}
		}
		//				System.out.println(" botssize = " + wld.ais.size());
//		Maino.getDelta(false);
		//		timer2 += delta;


		//		for (int a = 0; a < team1.size(); a++) {
		//			team1.get(a).think(delta);
		////			senddata(team1.get(a));
		//		}
		//
		//
		//		for (int a = 0; a < team2.size(); a++) {
		//			team2.get(a).think(delta);
		////			senddata(team2.get(a));
		//		}
		//				Maino.getDelta();
	}



	//	@Override
	//	public void run() {
	//		long start = System.currentTimeMillis(), end;
	//		//				int delta;
	//		delta = 0;
	//		while (true) {
	////			Maino.getDelta();
	//			start = System.currentTimeMillis();
	////			System.out.println("Thread  l delta = " + (end - start));
	//			
	//			//					+ deltap;
	//
	//						if (delta >= 1) 
	////						{
	//			//				if (gamesadyes) {
	//
	////			manage(delta);
	//
	////							synchronized (t) {
	////								try {
	//									manage(delta);
	//			//						t.wait();
	////								} catch (InterruptedException e) {
	////									e.printStackTrace();
	////								}
	////							}
	//
	//			//					gamesadyes = false;
	//			//				}
	//						
	////			System.out.println("Thread delta = " + delta);
	//			//				deltap = 0;
	//			end = System.currentTimeMillis();
	//			delta = (int) (end - start);
	////			Maino.getDelta();
	//			//			}
	//		}
	//
	//	}





	//	private void senddata(Enemy ent) {
	//		EnDPak gob = new EnDPak();
	//		gob.id = ent.id;
	//		gob.x = ent.x;
	//		gob.y = ent.y;
	//		gob.fdir = ent.fdir;
	//		gob.move = ent.move;
	//
	//		Maino.serv.broadcast(gob);
	//	}

}
