package com.mdked.owars.core;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.geom.Rectangle;

import com.mdked.owars.core.werld.BuildA;
import com.mdked.owars.core.werld.Fraction;
import com.mdked.owars.core.werld.GraMap;
import com.mdked.owars.core.werld.RObj;
import com.mdked.owars.units.Bullet;
import com.mdked.owars.units.Enemy;
import com.mdked.owars.units.Entity;
import com.mdked.owars.units.Fighter;
import com.mdked.owars.units.Flag;
import com.mdked.owars.units.Musor;
import com.mdked.owars.units.Player;
import com.mdked.owars.units.Pregrada;
import com.mdked.owars.units.Respawn;
import com.mdked.owars.units.strat.Facility;

public class World implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2857071481881276242L;
	public Player plr;
	public int fids, worldbsize;
	public int ids;

	public static int kvx = 64;

	//	public byte[][] map;

	public ArrayList<Entity> obj, cobj, bullets;
	public ArrayList<RObj> robj;

	public ArrayList<Enemy> ais;
	//	public ArrayList<Respawn> spawns;

	public ArrayList<Fraction> fracs;

	public long seed;
	public Random randa;

	//	private boolean ochered;

	public transient GraMap	mape;
	//	private boolean chunksChanged;

	public World(boolean neo) {
		if (neo) {
			randa = new Random();
			seed = randa.nextLong();

			File map = new File("saves/" + Long.toString(seed)+ "/");
			map.mkdirs();

			randa = new Random(seed);

			ids = 0;
			fids = 0;

			ais = new ArrayList<>();

			cobj = new ArrayList<>();
			bullets = new ArrayList<>();

			obj = new ArrayList<>();

			robj = new ArrayList<>();

			//			spawns = new ArrayList<>();
			fracs =  new ArrayList<>();

			worldbsize = 128;
			//			generateCities(128, 0);	
			//
			//			generateOkrestn();
		}

		mape = new GraMap( this );

	}
	public void init() {

		Fraction plrf = new Fraction(++fids, "Player", true);
		Fraction botoac = new Fraction(++fids, "Evil Botsu");
		fracs.add(plrf);
		fracs.add(botoac);

		plr = new Player(plrf);
		addObj(plr, true, false, false);
	}

	public void initLoaded() {

		//		robj = new ArrayList<>();
		mape = new GraMap( this );
	}

	//	public void refreshChunks() {
	//		int pCX = (int) (plr.x/(worldbsize*16)), pCY = (int) (plr.y/(worldbsize*16));
	//		boolean got = false;
	//		for (int x = -1; x < 2; x++) 
	//			for (int y = -1; y < 2; y++	) {
	//				
	//				for (int a = 0; a < chunks.size(); a++) 
	//					if (pCX + x == chunks.get(a).x && pCY + y == chunks.get(a).y) {
	//						got = true;
	//						break;
	//					}
	//				if (!got) 
	//					generateChunk(pCX+x, pCY+y);
	//			}
	//
	//	}

	//	private void generateChunk(int i, int j) {
	//		// TODO Auto-generated method stub
	//
	//	}

	//	public void newWorld() {
	////		redPts = 0;
	////		bluPts = 0;
	////		killscore = 1000;
	//		ids = 0;
	////		botQ = 10;
	////		winpts = 500 * 1000;
	//
	//		generateWorld();
	//	}

	//	public void restart(int winTeam) {
	////		redPts = 0;
	////		bluPts = 0;
	//
	//		for (int a = 0; a < objcs.size(); a++) {
	//			switch (objcs.get(a).type) {
	//			case 0:
	//				spawnPlayer((Player) objcs.get(a), true);
	//				break;
	//			case 4:
	//				Flag fle = (Flag) objcs.get(a);
	//				fle.lastTeam = 0;
	//				DataPok flogo = new DataPok();
	//				flogo.type = 8;
	//				flogo.ent = fle;
	//
	////				Maino.serv.broadcast(flogo);
	//				break;
	//			}
	//		}
	//
	//		Maino.aim.clearo();
	//
	//		DataPok pk = new DataPok();
	//		pk.type = 10;
	//		pk.iOne = winTeam;
	////		Maino.serv.broadcast(pk);
	//	}

	public void checkGame(int delta) {

		moveBullets(delta);
		checkColli();
		checkPlrs(delta);
		chkMusor(delta);

		chkFacs(delta);


	}
	private void chkFacs(int delta) {
		for (int a = 0; a < cobj.size(); a++) {
			if (cobj.get(a).type >= 100)
			{
				Facility fec = (Facility) cobj.get(a);
				fec.checkFac(delta);
			}
		}
	}

	public Respawn getSpawn(Fraction team, Entity asker) {
		Facility closestFac = null;
		int dist = 100000;
		for (int a =0; a < team.facs.size(); a++ ) {
			if (!team.facs.get(a).built) continue;
			int ddist = (int) getDist(asker, team.facs.get(a) );
			if (ddist < dist) {
				//				System.out.println("closeWst spawn dist = " + ddist + " fac size "+ team.facs.size());
				closestFac = team.facs.get(a);
				dist = ddist;
			}
		}

		return closestFac.getRandSpawn();

	}

	public int[] getSpawnXY(Fraction team) {

		Respawn spawn = getSpawn(team, plr);
		int[] kek = {(int) spawn.x, (int) spawn.y};
		return kek;

	}

	public boolean dealPlayerDeath(Player plier) {
		Fighter fi = plier.sqd.getLastSoldir();
		if (fi != null) {

			//			System.out.println(" delpD type= " + fi.type );

			remObj(fi);
			plier.takeBody(fi);
			plier.sqd.refreshFtgts(plier);
			return true;
		}
		else
			return false;
	}

	public void spawnPlayer(Player plier, boolean respawne) {

		if (!respawne && dealPlayerDeath(plier)) {

		}
		else {


			int[] resp = getSpawnXY(plier.team);

			if (getDist(plier, getSpawn(plier.team, plier) ) > 1000) {
//				Maino.aim.clearo();
				deleteFar(0);
				
			}

			plier.stun(1000);


			plier.x = resp[0];
			plier.y = resp[1];
			plier.hp = 100;

			plier.giveWeapon(plier.deswpn);

			plier.reload();

		}

		//		if (Maino.server) {
		//			DataPok pk = new DataPok();
		//			pk.type = 41;
		//			pk.ent = plier;
		//			System.out.println(" plier resp send " + plier);
		//			Maino.serv.broadcast(pk);
		//
		//			WPok wep = new WPok();
		//			wep.wpn = plier.wpn;
		//
		//			Maino.serv.sendtoplayer(plier.connId, wep);
		//		}
	}


	public void placeCrate(int x, int y) {
		Pregrada go = new Pregrada(x, y, 0);
		addObj(go, false, false, false);
	}

	public void placeTree(int x, int y) {
		Pregrada go = new Pregrada(x, y, 1);
		addObj(go, false, false, false);
	}
	public void generateBase(Facility base) {
		System.out.println("gb");
		generateBase((int) (base.x), (int) (base.y), base.owner);
	}
	public void generateBase(int x, int y, Fraction team) {
		Facility fac = new Facility(x, y, 200, team, 10)
				, cha = (Facility) addCObj(fac);
		if (cha != null)
			fac = cha;

		addRobr(x, y, 3);


		Entity clr = new Entity(x, y);
		clr.xWidth = 20*World.kvx;
		clr.yWidth = 20*World.kvx;
		clearPlacement(clr);

		for (int xx = 0; xx < 20; xx++) 
			for (int yy = 0; yy < 20; yy++) {

				if (cha == null) {

					if (yy == 3 && xx == 3 || xx == 16 && yy == 3 ||
							yy == 16 && xx == 3 || yy == 16 && xx == 16 	)
						fac.addWaypoint(x + (xx * World.kvx), y + (yy * World.kvx));

					if ( yy == 10 && xx == 10) {
						//						Respawn fl = new Respawn(x + (xx * World.kvx), y + (yy * World.kvx),
						//								team);
						fac.addSpawn(x + (xx * World.kvx), y + (yy * World.kvx));
						//						spawns.add(fl);
					}
				}

				if (yy > 0 && yy < 9 || yy > 12 && yy < 20 )
					if (xx == 1 || xx == 19)
						placeCrate(x + (xx * World.kvx), y + (yy * World.kvx));

				if (xx > 0 && xx < 9 || xx > 12 && xx < 20 )
					if (yy == 1 || yy == 19)
						placeCrate(x + (xx * World.kvx), y + (yy * World.kvx));

				if (yy > 7 && yy < 14 )
					if (xx == 4 || xx == 16)
						placeCrate(x + (xx * World.kvx), y + (yy * World.kvx));

				if (xx > 7 && xx < 14 )
					if (yy == 4 || yy == 16)
						placeCrate(x + (xx * World.kvx), y + (yy * World.kvx));
				if (xx == 10 && yy == 10 )
					placeFlag(x + (xx * World.kvx), y + (yy * World.kvx), fac.owner.skin);

			}
		fac.setBuild();
	}

	public void placeFlag(int x, int y, int skin) {

		Flag fl = new Flag(x , y, skin);
		addObj(fl, true, false, false);

	}


	//	public void generateWorld() {
	//
	//		generateCity((worldbsize/5)/2, (worldbsize/5)/2, 30);
	//
	//		placeFlag(2, worldbsize - 10);
	//		placeFlag(worldbsize - 10, 2);
	//		placeFlag(worldbsize/2 - 5, worldbsize/2 - 5);
	//
	//		placeResp(2, 2, 1, false);
	//		placeResp(worldbsize-8, worldbsize -10, 2, true);
	//
	//		for (int bx = 0; bx < worldbsize; bx ++	) 
	//			for (int by = 0; by < worldbsize; by++) {
	//				if (by == 0 || by == worldbsize -1|| bx == 0 || bx == worldbsize-1) 
	//					placeTree(bx * 16, by * 16);
	//
	//				if (Math.random() <= 0.05d ) {
	//					if (  bx < 10 && by < 10 || 
	//							bx > worldbsize - 11 && by > worldbsize - 11)
	//						continue;
	//					placeTree(bx * 16, by * 16);
	//				}
	//
	//			}
	//
	//
	//
	//
	//
	//
	////		plr = new Player(1);
	//		addObj(plr, true);
	//		//		plr.giveWeapon((int) (Math.random()*2));
	//		spawnPlayer(plr, true);
	//	}

	private void pInAr(Entity ent) {
		switch (ent.type) {
		case 0:
			// players.add(ent);
			break;

		case 2:
			bullets.add(ent);
			// bullets.add(ent);
			break;

		case 4:
			// flags.add(ent);
			break;

			//		case 5:
			//			spawns.add((Respawn) ent);
			//			break;

		case 6:
			Musor ms = (Musor) ent;
			//			switch (ms.vid) {
			//			case 2:
			if (ms.vid == 2)
				ms.setAnim(Maino.dedup1);

			break;

		}
	}

	//	public Entity getIdobj(int id) {
	//		for (int a = 0; a < objcs.size(); a++) {
	//			if (objcs.get(a).id == id) {
	//				//				if (Maino.client)
	//				//					System.out.println(" getIdobj " + id + " a = " + a);
	//
	//				return objcs.get(a);
	//			}
	//		}
	//		return null;
	//	}

	public ArrayList<Entity> getObjects() {

		//		if (chunksChanged) {

		//			ArrayList<Entity> ret = new ArrayList<>();
		//			for (int a =0; a < chunks.size(); a++) 
		//				ret.addAll(chunks.get(a).objcs);
		//			return ret;
		////		}

		return obj;
	}

	public boolean chObjPlace(Entity ent) {
		//				boolean clear = true;
		ArrayList<Entity> objcs = getObjects();

		for (int a =0; a < objcs.size(); a++) {
			Entity cheka = objcs.get(a);

			if (ent.x+ent.xWidth >= cheka.x && ent.x < cheka.x+cheka.xWidth && ent.y+ent.yWidth >= cheka.y && ent.y < cheka.y+cheka.yWidth) {

				if (ent.type == 3 && cheka.type == 9) return true;

				return false;
			}
		}
		return true;
	}

	//	public Chunk getChunk(Entity where) {
	//		return getChunk( (int)where.x, (int) where.y );
	//	}

	//	public Chunk getChunk(int x, int y) {
	//
	//		for (int a = 0; a < chunks.size(); a++) 
	//			if (x/(worldbsize*16) == chunks.get(a).x && y/(worldbsize*16) == y) 
	//				return chunks.get(a);
	//
	//		return null;
	//
	//	}

	public Entity addCObj(Entity ent) {
		Entity ask = getConstantObj(ent);

		if ( ask == null) {
			//		System.out.println("add cobj " + ent.type);
			if (ent.type >= 100 )
			{
				Facility fac = (Facility) ent;
				fac.owner.facs.add(fac);

			}
			cobj.add(ent);
			return null;
		} else
			return ask;
	}

	//Connection conn,
	public void addObj(Entity ent,  boolean over, boolean constant, boolean clear) {
		if (!over && !chObjPlace(ent)) return;

		if (clear)
			clearPlacement(ent);

		if (ent.type <= 8) 
			addRobr(ent.x, ent.y, 0);

		//		ent.id = ++ids;
		//		System.out.println(" add obj = " + ent.id + " obj- " + ent);


		//		getChunk(ent).objcs.add(ent);


		if (constant)
			addCObj(ent);

		obj.add(ent);
		//		chunksChanged = true;
		// bull
		pInAr(ent);
		//
		// if (ent.type ==2)
		// if (ent.type == 4)


		//		if (Maino.server) {
		//			DataPok sund = new DataPok();
		//
		//			switch (ent.type) {
		//			case 0:
		//				// player
		//				sund.type = 31;
		//				sund.ent = (Player) ent;
		//				Player plrx = (Player) ent;
		//				//				plrx.giveWeapon((int) (Math.random()*2));
		//
		//				if (conn != null) {
		//					DataPok otv = new DataPok();
		//					otv.type = 3;
		//					otv.ent = ent;
		//					conn.sendTCP(otv);
		//					Player plro = (Player) ent;
		//					plro.connId = conn.getID();
		//
		//					//					ConPlr po = new ConPlr();
		//					//					po.conId = conn.getID();
		//					//					po.player = ent;
		//					//					Maino.serv.conplr.add(po);
		//
		//					WPok wep = new WPok();
		//					wep.wpn = plrx.wpn;
		//					conn.sendTCP(wep);
		//
		//				}
		//				break;
		//
		//			case 2:
		//				Bullet buls = (Bullet) ent;
		//				buls.pregs = new ArrayList<>();
		//			case 1:
		//			case 3:
		//			case 4:
		//			case 6:
		//				sund.type = 5;
		//				sund.ent = ent;
		//				break;
		//			}
		//			Maino.serv.broadcast(sund);
		//
		//		}

	}


	private void clearPlacement(Entity ent) {
		ArrayList<Entity> deletet = new ArrayList<>();
		for (int a = 0; a < obj.size(); a++) {
			//obj.get(a).type < 8 && 
			if (obj.get(a).type   > 1 && obj.get(a).type < 8 && obj.get(a).id != ent.id && colided(ent, obj.get(a))) {
				//    		 System.out.println(obj.get(a).type);

				deletet.add(obj.get(a));
				//    		 remObj(obj.get(a));
			}

		}
		for (int b = 0; b < deletet.size(); b++)
			remObj(deletet.get(b));

	}

	private void addRobr(float x, float y, int type) {
		RObj rer = new RObj(x, y, type);
		robj.add(rer);
	}

	//	public boolean checkPColl() {
	//		for (int a = 0; a < objcs.size(); a++) {
	//			Entity colider = objcs.get(a);
	//			switch (colider.type) {
	//
	//			case 3:
	//				if (plr.x + 14 >= colider.x && plr.x <= colider.x + 14
	//				&& plr.y + 16 >= colider.y && plr.y <= colider.y + 14)
	//					return false;
	//				break;
	//
	//			}
	//		}
	//		return true;
	//	}

	private Respawn chekRespReach(Fighter fito) {
		Respawn spn = getSpawn(fito.team, fito);
		if (getDist(fito.x, spn.x, fito.y, spn.y) < 300) 
			return spn;

		return null;
	}

	public void checkPlrs(int delta) {
		ArrayList<Entity> objcs = getObjects();

		for (int a = 0; a < objcs.size(); a++) {
			if (objcs.get(a).type == 0) {
				Player pler = (Player) objcs.get(a);
				pler.activity(delta);

				//FIXME Player asq
				//				if (pler.askSqdmt()  ) {
				//					Respawn wertospn =	chekRespReach(pler);
				//					if (wertospn != null) {
				//						Maino.aim.addSqdMate(pler, wertospn);
				//					}
				//				}



			}
		}
	}

	public void chkMusor(int delta) {
		ArrayList<Entity> objcs = getObjects();
		for (int a =0; a < objcs.size(); a++) 
			if (objcs.get(a) != null && objcs.get(a).type == 6) {
				Musor mus = (Musor) objcs.get(a);

				mus.lifespan += delta;
				if (mus.z > 0) 
					mus.mova(delta);
				else mus.z = 0;

				//				if (Maino.server) {
				//					DataPok gob = new DataPok();
				//					gob.type = 6;
				//					gob.ent = mus;
				//					Maino.serv.broadcast(gob);
				//				}

				if (mus.lifespan > 30000) remObj(mus);
			}

	}

	public Player getPlr(Entity get) {
		ArrayList<Entity> objcs = getObjects();
		for (int a = 0; a < objcs.size(); a++)
			if (objcs.get(a).id == get.id)
				return (Player) objcs.get(a);

		return null;

	}

	//	public boolean reyCast(int xs, int ys, int xe, int ye) {
	//		Ray ray = new Ray(xs, ys, xe, ye);
	//		
	//	}

	private boolean chekAbl(int x, int y, ArrayList<BuildA> able) {
		for (int a = 0; a < able.size(); a++) 
			if (able.get(a).x == x && able.get(a).y == y ) return false;
		return true;
	}

	private void placeCitiSh(Pregrada preg, ArrayList<BuildA> able, int num) {
		int size = 320;
		switch (preg.ctype) {
		case 90:
			if (chekAbl((int)preg.x-size,(int) preg.y, able))
				able.add(new BuildA((int)preg.x-size,(int) preg.y, false, false, num));
			if (chekAbl((int)preg.x+size,(int) preg.y, able))
				able.add(new BuildA((int)preg.x+size,(int) preg.y, false, false, num));
			if (chekAbl((int)preg.x,(int) preg.y-size, able))
				able.add(new BuildA((int)preg.x,(int) preg.y-size, true, true, num));
			if (chekAbl((int)preg.x,(int) preg.y+size, able))
				able.add(new BuildA((int)preg.x,(int) preg.y+size, true, true, num));
			break;
		case 91:
			if (chekAbl((int)preg.x-size,(int) preg.y, able)) able.add(new BuildA((int)preg.x-size,(int) preg.y, true, false, num));
			if (chekAbl((int)preg.x+size,(int) preg.y, able)) able.add(new BuildA((int)preg.x+size,(int) preg.y, true, false, num));
			if (chekAbl((int)preg.x,(int) preg.y-size, able)) able.add(new BuildA((int)preg.x,(int) preg.y-size, true, true, num));
			if (chekAbl((int)preg.x,(int) preg.y+size, able)) able.add(new BuildA((int)preg.x,(int) preg.y+size, true, true, num));
			break;
		case 92:
			if (chekAbl((int)preg.x-size,(int) preg.y, able)) able.add(new BuildA((int)preg.x-size,(int) preg.y, true, false, num));
			if (chekAbl((int)preg.x+size,(int) preg.y, able)) able.add(new BuildA((int)preg.x+size,(int) preg.y, true, false, num));
			if (chekAbl((int)preg.x,(int) preg.y-size, able)) able.add(new BuildA((int)preg.x,(int) preg.y-size, false, true, num));
			if (chekAbl((int)preg.x,(int) preg.y+size, able)) able.add(new BuildA((int)preg.x,(int) preg.y+size, false, true, num));
			break;
			//		case 100:
			//			break;
		}
		//		System.out.println(" Placed city shit type- " + preg.ctype + " at " + preg.x + "x" + preg.y);

		addObj(preg, true, false, true);

	}

	/**
	 * XB - YB - *80
	 * @param xB
	 * @param yB
	 * @param size
	 */
	public void generateCity(int x, int y, int size) {
		Random bgR = new Random();
		bgR.setSeed(getXYSeed(x,y));

		addRobr(x, y, 1);

		Facility fac = new Facility(x,y, 100, getRndNPF(x,y), size);

		//		if (plr.tcity == null) plr.tcity = fac;
		//		boolean alreadygot;
		//		alreadygot = 
		//				;
		//		System.out.println(" Citygen addcobj xy " + fac.x + "x" + fac.y);
		Facility initc = (Facility) addCObj(fac) ;


		//		if (initc == null) 
		//			System.out.println(" nulle sity ");
		//		else
		//			System.out.println("  inits " + initc.spawns.size());


		//			fac.spawns.add( new Respawn(x, y ,
		//					fac.owner));
		//		}

		ArrayList<BuildA> ables = new ArrayList<BuildA>();

		BuildA sb = new BuildA((int)x,(int) y, true, false, 0);
		ables.add(sb);
		placeCitiSh(new Pregrada(x, y, 91),ables, 0);
		if (initc == null) {
			fac.addSpawn(x, y);
			fac.addWaypoint(x+40, y+40);
		}



		//		addObj(fl, true, true, false);

		sb.built = true;

		int sizo = 0;
		while (sizo < size) {
			for (int a = 0; a < ables.size(); a++) 
				if (bgR.nextFloat() < 0.3f) {
					BuildA bla = ables.get(a);
					if (bla.built) continue;

					if (bla.roadOnly) {
						//2 bgR.nextInt(3) + 1
						if (bla.num >= 2) {
							placeCitiSh(new Pregrada(bla.x, bla.y, 91),ables, 0);
							if (initc == null) {
								//								System.out.println(" nulle sity add spwn wp");
								fac.spawns.add( new Respawn(bla.x, bla.y, fac));
								fac.addWaypoint(bla.x, bla.y);
							}
						}
						else {
							if (bla.vert)
								placeCitiSh(new Pregrada(bla.x, bla.y, 90),ables, ++bla.num);
							else
								placeCitiSh(new Pregrada(bla.x, bla.y, 92),ables, ++bla.num);
						}
					} else {
						//house
						placeCitiSh(new Pregrada(bla.x, bla.y, 2),ables, 0);
						sizo++;
					}
					bla.built = true;
					break;
				}
		}
		//		if (initc == null) {
		//		 Faciity initsc = (Facility) addCObj(fac);
		//		 System.out.println(initsc.spawns.size() + " wp " + initsc.waypoints.size());
		//				
		//		}
		if (initc != null) {
			//			Facility fec = (Facility) initc;
			initc.initSoldSpawn();
		}
		else 
			fac.initSoldSpawn();
		//		System.out.println("City is done");
		fac.setBuild();
	}

	private Fraction getRndNPF(int x, int y) {

		if (fracs.size() > 2)
		{
			Random rnd = new Random(getXYSeed(x, y));
			int ranmx = fracs.size() - 1;
			return fracs.get(rnd.nextInt(ranmx)+1);

		}
		else 
			return fracs.get(1);
	}

	public boolean checkProb(Bullet bula, Pregrada pregrad) {
		//		System.out.println(" start prob " + System.currentTimeMillis());

		if (bula.lifetime < 250 && pregrad.ukrit) return true;

		//		if ( bula.pregs.size() < 3) {
		if (bula.chekpreg(pregrad) ) {

			float proz = (float) (Math.random()*100f);
			if ( proz >= pregrad.prozukr) {
				//				System.out.println("proz = " + proz + " ukrit = " + pregrad.prozukr);
				bula.pregs.add(pregrad);
				//				System.out.println(" ret true");

				return true;
			}


			float prozpen =
					50f + ((bula.faza.penetr - pregrad.penrate));

			if (Math.random()*100f <= prozpen) {
				bula.lifetime += 1000;
				bula.speed *=0.7f;
				float rico = (float) (-0.5f + (Math.random()*1.1f));
				bula.rot += rico;
				bula.pregs.add(pregrad);
				Maino.playSound(bula.x, bula.y, 3);
				return true;
			} else {
				//				System.out.println(" ret false");
				Maino.playSound(bula.x, bula.y, 4);
				return false;
			}

		}
		//		}
		//		System.out.println(" ret end");
		return true;
	}

	public boolean colided(Entity one, Entity two) {
		//		if(	one.x + one.xxWidth + one.xWidth >= two.x + two.xxWidth && one.x + one.xxWidth <= two.x + two.xxWidth + two.xWidth
		//				&& one.y + one.yyWidth + one.yWidth >= two.y + two.yyWidth && one.y + one.yyWidth <= two.y + two.yWidth )
		//			return true;
		//		else return false;
		Rectangle oner = new Rectangle(one.x+one.xxWidth, one.y+ one.yyWidth, one.xWidth, one.yWidth),
				twor = new Rectangle(two.x+two.xxWidth, two.y+ two.yyWidth, two.xWidth, two.yWidth);

		return oner.intersects(twor);
	}

	public int checkCollGetProb(Entity colider) {
		ArrayList<Entity> objcs = getObjects();
		for (int a = 0; a < objcs.size(); a++) {
			Entity mo = objcs.get(a);
			if ( getDist(colider, mo) < 65 && colided(colider, mo)) {
				switch (mo.type) {
				case 3:
				case 7:
				case 8:
					Pregrada pd = (Pregrada) mo;
					return pd.penrate;
				}
			}
		}
		return 0;
	}

	public void bulletHit(Fighter faza, Bullet bula, Entity obj) {
		{
			switch (obj.type) {
			case 0:
			case 1:

				Fighter plur = (Fighter) obj;
				if (faza.id == plur.id ||
						faza.team == plur.team) return;

				int dmg = (int) ((bula.faza.mindmg + ((bula.damage- bula.faza.mindmg) * Math.random()))*bula.getDmgx());
				//				if (dmg < bula.faza.mindmg) dmg = bula.faza.mindmg;
				plur.hp -= dmg;
				//			System.out.println(" damage = " +  dmg + " xx " + bula.getDmgx());

				if (plur.hp <= 0) {
					//					Fighter killah = (Fighter) getIdO(bula.faza.owner);

					//					if (faza.team == 1)
					//						redPts += killscore;
					//					if (faza.team == 2)
					//						bluPts += killscore;

					plur.die();
					switch (plur.type){
					case 0:
						spawnPlayer((Player) plur, false);
						break;
					case 1:
						Maino.aim.killBot(plur.id);
						remObj(plur);
						break;
					}

					//					Player killah = (Player) bula.faza.owner;
					//					killah.score++;
					bula.hit = true;
					remObj(bula);

					return;
				}
				//
				////				DataPok sendpl = new DataPok();
				//				if (plur.type == 0)
				//					sendpl.type = 41;
				//				else 
				//					sendpl.type = 6;
				//
				//				sendpl.ent = plur;
				// System.out.println(" send 41 freeze = " +
				// plur.frozen);
				//				Maino.serv.broadcast(sendpl);
				bula.hit = true;
				remObj(bula);
				break;

			case 3:
			case 7:
			case 8:
				if (!checkProb(bula,(Pregrada) obj)) { 
					//					System.out.println("delete");
					bula.hit = true;
					remObj(bula);
				}


				break;
			}
		}
	}

	public static synchronized double getDist(Entity one, Entity two) {
		return getDist(one.x, two.x, one.y, two.y);
	}
	public static double getDist(float x1, float x2, float y1, float y2) {
		return Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) ) ;
	}

	public void checkColli() {
		
		for (int a = 0; a < bullets.size(); a++) {
			
			Bullet bula = (Bullet) bullets.get(a);
			Fighter faza = (Fighter) getIdO(bula.faza.owner);
			
			if (faza != null)
				for (int b = 0; b < obj.size(); b++) {
					if (bula.hit) break;
					
					Entity colider = obj.get(b);
					if (colider == null || getDist(bula, colider) > 450) continue;
					
					if (bula.x + bula.xWidth/2 >= colider.x+colider.xxWidth && bula.x - bula.xWidth/2 <= colider.x +colider.xxWidth + colider.xWidth
							&& bula.y + bula.yWidth/2 >= colider.y + colider.yyWidth
							&& bula.y - bula.yWidth/2 <= colider.y + colider.yyWidth + colider.yWidth) {
						bulletHit(faza, bula, colider);

					}
				}
		}

	}
	//			}
	//		}
	//	}

	//	public void addCObj(Entity ent) {
	//		//		System.out.println(" new obj " + ent);
	//		for (int a = 0; a < objcs.size(); a++)
	//			if (ent != null && objcs.get(a) != null && objcs.get(a).id == ent.id)
	//				return;
	//
	//		objcs.add(ent);
	//
	//		pInAr(ent);
	//
	//	}

	// public synchronized void addForRem(Entity ent) {
	// delque.add(ent);
	// System.out.println("delque = " + delque.size());
	//
	// }

	// public void remQue() {
	// if (delque.size() > 0)
	// System.out.println("delque = " + delque.size());
	// for (int a = 0; a < delque.size(); a ++ ) {
	// remObj(delque.get(a));
	// }
	// delque.clear();
	// }

	//	public void remRObj(RObj ent) {
	//		drobj.add(ent);
	//	}

	//	public void remCObj(Entity ent) {
	//		dcobj.add(ent);
	//	}

	public void remObj(Entity ent) {
		//		dobj.add(ent);
		remObj(ent.id);
	}

	//	public void flushTrash() {
	//		for (int a = 0; a < dobj.size(); a++) 
	//			remObj(dobj.get(a).id);
	//		dobj.clear();
	//		
	//		for (int a = 0; a < drobj.size(); a++) 
	//			robj.remove(drobj.get(a));
	//		drobj.clear();
	//		
	//		for (int a = 0; a < dcobj.size(); a++) 
	//			cobj.remove(dcobj.get(a));
	//		dcobj.clear();
	//			
	//	}

	public int getXYSeed(int x, int y) {

		int 
		//		xr = x < 0 ? Math.abs(x) : x,
		//				yr = y < 0 ? Math.abs(y) : y,
		ret = (int) ((x*1000 + y) * seed);
		//				if (ret < -2147483647l)
		//				System.out.println(ret);

		return ret;
	}

	public void generateCities(int out, int in) {
		Random bgR = new Random();

		//		int sizec = 64, inbord = 50;
		int sx =(int) (plr.x/kvx - (out/2)), sy = (int) (plr.y/kvx - (out/2)) ;
		//		System.out.println("starting Generating city from " + sx +"x " + sy);
		for (int x = sx; x < sx + out; x++) {
			for (int y = sy; y < sy + out; y++) {
				int dist = (int) getDist(x, plr.x/kvx, y, plr.y/kvx);
				if ( dist < in  ) {

					continue;
				}
				//				System.out.println(" dist "  + dist);
				bgR.setSeed(getXYSeed(x,y));
				bgR.nextFloat();

				int wX = ((x*kvx)/320)*320, wY = ((y*kvx)/320)*320;

				if ( bgR.nextFloat() <= 0.00009) {
					boolean chtl = checkObj(wX,wY, true, 1);
					//										System.out.println(" check " + checkObj(wX, wY, false)  +" "+ chtl);
					//					System.out.println("Tryin Generating city at " + x +"x " + y + " wxy " + wX + "x" + wY + " check " + chtl);

					if (!chtl && noFacInDist(wX, wY, 6000)) {
						System.out.println("  Generating city at " + x +"x " + y + " wxy " + wX + "x" + wY + " check " + chtl);

						generateCity(wX, wY, bgR.nextInt(24) + 3);
						//						System.out.println(" check " + checkObj(wX, wY, false));
					}
				}



			}
		}
	}
	private boolean noFacInDist(int xs, int ys, int dist) {
		Entity starti = new Entity(xs,ys);

		//, int factype && cobj.get(a).type == factype

		if (getConstantObj(starti) != null) return true;

		for (int a = 0 ; a < cobj.size(); a++) 
			if (getDist(starti, cobj.get(a)) < dist	)
				return false;

		return true;
	}
	public void generateForest(int x,int y, int size) {
		addRobr(x, y, 2);
		//		System.out.println("Forest " + x + "x" + y);

		Random bgR = new Random();

		for (int ax = (x/kvx) -(size/2); ax < (x/kvx) + size/2; ax++) 
			for (int ay = (y/kvx) - (size/2); ay <  (y/kvx) + size/2; ay++	) {

				bgR.setSeed(getXYSeed(ax,ay));
				bgR.nextFloat();

				int wX = ax*kvx, wY = ay*kvx;
				float proz = (float) (((size/2 - getDist(ax, x/kvx, ay, y/kvx)) / (size/1.7f)) * 0.2f);
				//				System.out.println("proz = " + proz + " dist = " + getDist(ax, x, ay, y) + " size = " + size);

				if ( bgR.nextFloat() <= proz)
					if (!checkObj(wX,wY, false, -1)) 
						placeTree(wX, wY);



			}
	}

	public void generateOkrestn() {
		Random bgR = new Random();
		int sizec = 180;
		int sx =(int) (plr.x/kvx - (sizec/2)), sy = (int) (plr.y/kvx - (sizec/2)) ;
		//		System.out.println("strt");
		for (int x = sx; x < sx + sizec; x++) {
			for (int y = sy; y < sy + sizec; y++) {
				bgR.setSeed(getXYSeed(x,y));
				bgR.nextFloat();

				int wX = x*kvx, wY = y*kvx;

				if ( bgR.nextFloat() <= 0.0001)
					if (!checkObj(wX,wY, false, 2)) 
						generateForest(wX, wY, bgR.nextInt(64) + kvx);
				//								placeTree(wX, wY);
				//
				//
				//
			}
		}
	}

	public void remObj(int id) {
		// if (Maino.client) {
		// System.out.println(" REMOVING OBJ objID- " + ent.id
		Entity obke = getIdO(id);
		if (obke != null) {
			if (obke.type == 1)
				Maino.aim.remove(obke.id);

			if (obke.type == 2)
				bullets.remove(obke);

			//			if (Maino.server && obke.type != 6) {
			//				if (obke.type == 0) 
			//					System.out.println(" Deleting player!!!!!!!");
			//
			//				RemOPok pk = new RemOPok();
			//				pk.id = id;
			//				//			pk.ent = ent;
			//				Maino.serv.broadcast(pk);
			//				if (obke.type == 1)
			//					Maino.aim.remove(obke.id);
			//			}

			//		if (checkImen(ent)) 
			//			getChunk(obke).objcs.remove(obke);
			getObjects().remove(obke);
			//			chunksChanged = true;

		}
	}


	public Entity getIdO(int id) {
		ArrayList<Entity> objcs = getObjects();
		for (int a = 0; a < objcs.size(); a++) {
			if (objcs.get(a) == null) continue;

			if ( objcs.get(a).id == id)
				return objcs.get(a);
		}
		return null;

	}

	//	public Player makeNewPlayer(Connection conn) {
	//
	//		int tm = 0;
	//		if (ochered)
	//			tm = 1;
	//		else
	//			tm = 2;
	//		ochered = !ochered;
	//
	//		Player ploor = new Player(tm);
	//		addObj(ploor, conn, true);
	//
	//		spawnPlayer(ploor, true);
	//
	//
	//
	//		return ploor;
	//	}

	public void moveBullets(int delta) {
		//		ArrayList<Entity> objcs = getObjects();
		for (int a = 0; a < bullets.size(); a++) {
			//			if (objcs.get(a).type == 2) {
			Bullet bul = (Bullet) bullets.get(a);
			bul.moveRot(delta);
			//				if (bul.x < 0 || bul.x > 640 || bul.y < 0 || bul.y > 480)
			//					remObj(bul);
			if (bul.getDmgx() <= 0.3f) {
				remObj(bul);
				continue;
			}



			//				if (Maino.server) {
			//					DataPok gob = new DataPok();
			//					gob.type = 6;
			//					gob.ent = bul;
			//					Maino.serv.broadcast(gob);
			//				}
		}
	}
	//	}

	//	public void manageIncEnt(Entity bula) {
	//		Entity koko = getIdO(bula);
	//		if (koko != null)
	//			switch (bula.type) {
	//
	//			//				case 1:
	//			//					//					System.out.println(" ena");
	//			//					Enemy en = (Enemy) objcs.get(a);
	//			//					en.update((Enemy) bula);
	//			//					return;
	//
	//			case 2:
	//				//bull
	//
	//				Bullet bul = (Bullet) koko;
	//				//				if (bul.id == bula.id)
	//				bul.updato((Bullet) bula);
	//				return;
	//
	//
	//
	//			case 6:
	//				//musor
	//				Musor mus = (Musor) koko;
	//				//				if (bula.id == mus.id) {
	//				mus.updato((Musor) bula);
	//				return;
	//			}
	//
	//	}

	//	public void managePlr(Player plr2, int type) {
	//		if (plr2 == null) return;
	//		// objcs
	//		//		System.out.println(" plr2 = " + plr2 + " kek");
	//		if (plr2.id == plr.id ) {
	//			if (type == 41)
	//				plr.updato(plr2, type);
	//			return;
	//		}
	//		ArrayList<Entity> objcs = getObjects();
	//
	//		for (int a = 0; a < objcs.size(); a++) {
	//			if (objcs.get(a).type == 0) {
	//
	//				Player plor = (Player) objcs.get(a);
	//				if (plor.id == plr2.id) {
	//					//					if (Maino.server && plor.frozen)
	//					//						return;
	//
	//					plor.updato(plr2, type);
	//				}
	//
	//			}
	//		}
	//	}

	public Entity getIdO(Entity ent) {
		return getIdO(ent.id);
	}

	//	public void dealFlg(Flag ent) {
	//
	//
	//		//			System.out.println();
	//		Entity onb = getIdO(ent);
	//		if (onb != null) {
	//			Flag cht = (Flag) onb;
	//			cht.lastTeam = ent.lastTeam;
	//			// System.out.println(" flagcome " + cht.lastTeam + " xx " +
	//			// ent.lastTeam);
	//		}
	//
	//		// for (int b = 0; b < flags.size(); b++) {
	//		// if (flags.get(b).id == ent.id) {
	//		// Flag cht = (Flag) flags.get(b);
	//		// cht.lastTeam = ent.lastTeam;
	//		// // System.out.println(" flagcome " + cht.lastTeam + " xx " +
	//		// ent.lastTeam);
	//		// }
	//		// }
	//
	//	}

	//	public void sendSP() {
	//		DataPok sp = new DataPok();
	//		sp.type = 9;
	//		sp.iOne = bluPts;
	//		sp.iTwo = redPts;
	//		Maino.serv.broadcast(sp);
	//	}

	//	public void dealSP(DataPok pk) {
	//		bluPts = pk.iOne;
	//		redPts = pk.iTwo;
	//	}

	public synchronized boolean checkOColl(Entity mo) {
		ArrayList<Entity> objcs = getObjects();

		for (int a = 0; a < objcs.size(); a++) {
			
			Entity colider = objcs.get(a);
			
			if (colider == null || colider.id == mo.id) continue;
			if (getDist(mo, colider) > 100) continue;
			
			switch (colider.type) {
			//			//			case 1:
			case 3:
			case 7:
			case 8:
				//				if (mo.x + mo.xxWidth + mo.xWidth >= colider.x + colider.xxWidth && mo.x + mo.xxWidth <= colider.x + colider.xxWidth + colider.xWidth
				//				&& mo.y + mo.yyWidth + mo.yWidth >= colider.y + colider.yyWidth && mo.y + mo.yyWidth <= colider.y + colider.yWidth ) 
				if(colided(mo, colider))
					return false;
				//				break;
				//				//		
				//			case 7:
				//				if (mo.x + mo.xWidth >= colider.x && mo.x <= colider.x + colider.xWidth
				//				&& mo.y + mo.yWidth >= colider.y - colider.yWidth && mo.y <= colider.y + colider.yWidth)  
				//					return false;
				//				break;
				//
			}
		}
		return true;
	}

	public boolean checkObj(int x, int y, boolean delete, int type) {
		//		int ret = -1;
		ArrayList<RObj> del = new ArrayList<RObj>();
		for (int a = 0; a < robj.size(); a++) 
			if (robj.get(a).x == x && robj.get(a).y == y) {
				if (type == -1) return true;
				if (robj.get(a).type == type)
					//					ret = robj.get(a).type;
					return true;
				//								System.out.println("xy " + x + "x" + y + "  cheking == " + ret + " - " + a);
				if (delete && robj.get(a).type != type)
					del.add(robj.get(a));
			}

		for (int a = 0; a < del.size(); a++) 
			robj.remove(del.get(a));

		return false;
	}



	public void deleteFar(int dist) {
		//2000
		ArrayList<Entity> obeji = new ArrayList<>();
		//		int dist = 2000;
		for (int a =0; a < obj.size(); a++) 
			if (getConstantObj(obj.get(a) ) == null && getDist(plr, obj.get(a)) > dist) {
				//				remObj(obj.get(a));
				obeji.add(obj.get(a));
				//		if (obj.get(a).type == 100) System.out.println(" ZASHKWAR");
			}


		for (int a =0; a < obeji.size(); a++) {
			if (obeji.get(a).type == 1) {
//				System.out.println(" soldatik ");
				Enemy botik = (Enemy) obeji.get(a);
				if (botik.facTarget != null) {
					botik.facTarget.deltFar();
//				System.out.println(" minus fac " + botik.facTarget.soldiers );
				}
//				Maino.aim.remAifrArr(botik);
			}
			remObj(obeji.get(a));
			//			obj.remove(obeji.get(a));

		}

		ArrayList<RObj> obejr = new ArrayList<>();

		for (int b =0; b < robj.size(); b++)
			if ( getDist(plr.x, robj.get(b).x, plr.y, robj.get(b).y) > dist )
				//				remRObj(robj.get(b));
				obejr.add(robj.get(b));

		//		System.out.println(" dFar robj size = " + obejr.size());

		for (int a =0; a < obejr.size(); a++) 
			robj.remove(obejr.get(a));
	}

	public Entity getConstantObj(Entity ent) {
		return isConstantObj((int)(ent.x),(int) (ent.y));
	}
	//FIXME kekekeke
	public Entity isConstantObj(int x, int y) {
		for (int a = 0; a < cobj.size(); a++) 
			if (cobj.get(a).x == x && cobj.get(a).y == y)
				return cobj.get(a);

		return null;
	}

	public void placeCObj() {
		for (int a =0; a < cobj.size(); a++) {
			Entity chk = cobj.get(a);
			// && getDist(plr, cobj.get(a)) < 1700
			if (chk.type == 200 && !checkObj((int) (chk.x), (int) (chk.y), false, 3)) 
				generateBase((Facility) cobj.get(a));
		}

	}
	public Fraction getPlrFr() {
		return plr.team;
	}
	public void deployLager() {
		placeConstruction(2201, plr, plr.team);

	}
	private void placeConstruction(int type, Entity where, Fraction owner) {
		placeConstruction(type, where.x+32, where.y+32, owner, 3);

	}
	private void placeConstruction(int type, float x, float y, Fraction owner, int size) {
		int px = (int) (x - x % 64), py = (int) (y - y % 64);
		Facility csite = new Facility(px, py, type, owner, size);
		//		if (addCObj(csite) == null )
		addObj(csite, false, true, true);

	}

	public void remCObj(Entity facility) {
		Facility facd = (Facility) facility;
		if (facd.owner != null)
			facd.owner.facs.remove(facd);

		cobj.remove(facility);

	}

	//	public void manageIncEDP(Poked pak) {
	//		EnDPak inc = (EnDPak) pak;
	//		Enemy peko = (Enemy) getIdO(inc.id);
	//		if (peko != null)
	//			peko.update( inc);
	//		return;
	//	}

	//	public void removePlayer(int connId) {
	//		for (int a = 0; a < objcs.size(); a ++) {
	//			if (objcs.get(a).type == 0) {
	//				Player pli = (Player) objcs.get(a);
	//				if (pli.connId == connId) 
	//					remObj(pli.id);
	//			}
	//		}
	//
	//	}

	// public void removePlr() {
	// remObj(plr);
	// plr = null;
	// }

}
