package com.mdked.owars.core.werld;

import java.util.ArrayList;

import org.newdawn.slick.Color;

import com.mdked.owars.core.Maino;
import com.mdked.owars.core.World;
import com.mdked.owars.units.*;
import com.mdked.owars.units.strat.Facility;

public class GraMap {

	public Color[][] mapo;
	private World wld;

	public GraMap(){

	}

	public GraMap(World wld) {
		this.wld = wld;
		mapo = new Color[wld.worldbsize][wld.worldbsize];
		//		MakeMap(wld);
	}

	public void MakeMap(World wld) {
		ArrayList<Entity> objcs = wld.getObjects();


		//		int mX = Maino.managerstate ? 4 : 1;

		int mapsize = mapo.length;
		int delitel = 128;
		if (Maino.managerstate) {

			int sx = (int) (wld.plr.x - (mapsize*delitel/2)),
					sy = (int) (wld.plr.y - (mapsize*delitel/2));

			for (int b = 0; b < wld.cobj.size(); b++) {
				Entity cobj = wld.cobj.get(b);

				Color cler = Color.gray;
				if (cobj.x >= sx && cobj.x < sx+mapsize*delitel &&
						cobj.y >= sy && cobj.y < sy+mapsize*delitel ) {
					switch (cobj.type) {
									
					case 100:
						//city
						Facility city = (Facility) cobj;
						
						cler = new Color(0.0f, 1.0f, city.owner.id*0.1f, 1f);
						
						
						break;
					case 200:
						//base
						Facility base = (Facility) cobj;
						cler = new Color(0.0f, base.owner.id*0.1f, 1.0f, 1f);
						break;
					}

					int xx = (int) ((cobj.x - sx) / delitel), yy = (int) ((cobj.y - sy ) / delitel);
					mapo[xx][yy] = cler;
				}
			}
			int xx = (int) ((wld.plr.x - sx) / delitel), yy = (int) ((wld.plr.y - sy ) / delitel);
			mapo[xx][yy] = new Color(1.0f, 0f, 0f, 1f);
		}
		else
			for (int a = 0; a < objcs.size(); a++) {
				Entity obj = objcs.get(a);
				if (obj == null) continue;
				int xx = (int) ((obj.x+1)/World.kvx);
				int yy = (int) ((obj.y+1)/World.kvx);
				if (xx < wld.plr.x/World.kvx - 32 && xx > wld.plr.x/World.kvx + 32) continue;
				if (yy < wld.plr.y/World.kvx - 32 && yy > wld.plr.y/World.kvx + 32) continue;

				int[] wtd =	Maino.wtd(obj, Maino.gcc);


				int x = wtd[0]/World.kvx + 57;
				int y = wtd[1]/World.kvx + 55;
				if (x < 0 || x >= mapo.length) continue;
				if (y < 0 || y >= mapo[0].length) continue;


				Color cler = Color.gray;

				switch (obj.type) {
				case 0:
				case 1:
					Fighter fito = (Fighter) obj;
					if (fito.team.id == 2) 
						cler = new Color(50,50,150);
					else 
						cler = new Color(150,50,50);
					break;

				case 3:
					cler = Color.orange;
					break;

				case 4:
					Flag fl = (Flag) obj;
					switch (fl.lastTeam) {
					case 0:
						cler = Color.lightGray;
						break;
					case 1:
						cler = Color.red;
						break;
					case 2:
						cler = Color.blue;
						break;
					}

					break;

				case 7:
					cler = Color.black;
					if (y+1 < mapo[0].length)
						mapo[x][y+1] = cler;
					break;
				case 8:
					Pregrada dome = (Pregrada) obj;
					if (dome.ctype >= 100) 
						drawQube(x, y, 5, 5, Color.black);
					continue;


				}

				if (obj.type == 3 || obj.type == 4 || 
						obj.type == 7 ||obj.type == 0 || obj.type == 1)
					mapo[x][y] = cler;
			}
	}

	private void drawQube(int x, int y, int width, int height, Color clr) {
		for (int xx = 0; xx < width; xx++) 
			for(int yy = 0; yy < height; yy++) 
				if (x+xx < mapo.length && y+yy < mapo[0].length)
					mapo[x+xx][y+yy] = clr;
	}

	public void refresh() {
		int xF = Maino.managerstate ? 2 : 1;
		mapo = new Color[wld.worldbsize*xF][wld.worldbsize*xF];
		MakeMap(wld);
		//		System.out.println(xF);
	}

}
