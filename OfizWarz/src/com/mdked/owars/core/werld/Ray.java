package com.mdked.owars.core.werld;

import com.mdked.owars.core.Maino;
import com.mdked.owars.core.World;
import com.mdked.owars.units.Entity;

public class Ray extends Entity{

	float dir;
	int dist, didone;
	
//	float oy, ox;
	
	public Ray(Entity one, Entity two) {
		this(one.x, one.y, two.x, two.y);
	}

	public Ray(float xs, float ys, float xe, float ye) {
		
//		ox = xs;
//		oy = ys;
		
		x = xs;
		y = ys;
		xWidth = 2;
		yWidth = 2;
// - Math.toRadians(90)
		dir = (float) (Math.atan2((y-ye), (x-xe))  - Math.toRadians(90)  );
		dist = (int) World.getDist(xs, xe, ys, ye);
	}

	public int rayCast() {
		int pregt =0;
		while (didone < dist) {
			pregt += Maino.wld.checkCollGetProb(this);
			makeStep();
		}
		return pregt;
	}

	public void makeStep() {
		int step = 64;
		didone += step;
		x += (step * Math.sin(dir));
		y -= (step * Math.cos(dir));

	}

//	public void drawRey( Graphics g, GameContainer gcc) {
//		int[] sh = Maino.wtd(ox, oy, gcc), endshit = Maino.wtd((float) (ox+(200 * Math.sin(dir))), (float)  (oy-(200 * Math.cos(dir))), gcc);
//		g.drawLine(sh[0], sh[1], endshit[0], endshit[1]  );
//	}

}
