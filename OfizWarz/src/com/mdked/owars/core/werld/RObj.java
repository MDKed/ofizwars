package com.mdked.owars.core.werld;

import java.io.Serializable;

public class RObj  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6395326224797451482L;
	public int x,y, type;
//	public boolean availbl;
	
	/**
	 * type 1 = city
	 * 2 = forest
	 * 3 = base
	 * @param x2
	 * @param y2
	 * @param type
	 */
	public RObj(float x2, float y2, int type) {
		x = (int) x2;
		y = (int) y2;
		this.type = type;
	}
}
