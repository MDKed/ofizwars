package com.mdked.owars.core.werld;

import java.io.Serializable;
import java.util.ArrayList;

import com.mdked.owars.units.strat.Facility;

public class Fraction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6126815539225228205L;
	public int id, skin, forces;
	public String name;
	public boolean plrFr;
	public float money;
	public ArrayList<Facility> facs;
	
//	public ArrayList<Fighter> population;
	
	public Fraction( int id, String name ) {
		this.id = id;
		this.name = name;
		
		skin = (int) (Math.random() * 2)+1;
		facs = new ArrayList<Facility>();
	}
	public Fraction( int id, String name, boolean player) {
		this(id,name);
		
		plrFr = true;
		
	}
	
//	public Fraction( int id, String name , int pop) {
//		this(id,name);
//		
//		for (int a = 0; a < pop; a++) {
//			Fighter kek = new Fighter();
//			population.add(kek);
//		}
//		
//	}

}
