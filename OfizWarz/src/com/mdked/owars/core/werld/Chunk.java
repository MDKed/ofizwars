package com.mdked.owars.core.werld;

import java.io.Serializable;
import java.util.ArrayList;

import com.mdked.owars.units.Entity;

public class Chunk implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4829317494644314915L;

	
	public int x,y;
	public ArrayList<Entity> objcs;
}
