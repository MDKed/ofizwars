package com.mdked.owars.core;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import com.mdked.owars.core.lvls.Main;
import com.mdked.owars.core.lvls.MainMenu;
import com.mdked.owars.units.Bullet;
import com.mdked.owars.units.Enemy;
import com.mdked.owars.units.Entity;
import com.mdked.owars.units.Fighter;
import com.mdked.owars.units.Flag;
import com.mdked.owars.units.Musor;
import com.mdked.owars.units.Pregrada;
import com.mdked.owars.units.strat.Facility;

public class Maino extends BasicGameState  {

	public static World wld;
	public static boolean 
	//	server, client, connected, 
	endgame, debug, debugsprite, started;
	//	public static Servor serv;
	//	public static Cleend clit;
	public static int timerone;
	public static int winteam;


	public static float zoom;

	public static AiManager aim;

	public static GameContainer gcc;

	public Animation runleft1,runlup1, runup1, runrup1, runright1, runrdown1, rundown1, runldown1, 
	idleup1, idleupr1, idler1, idlerd1, idled1, idledl1, idlel1, idlelu1,
	dedupr1, dedr1, dedrd1, dedd1, deddl1, dedl1, dedlu1,
	ffu1, ffur1, ffr1, ffrd1, ffd1, ffdl1, ffl1, fflu1,
	fbu1, fbur1, fbr1, fbrd1, fbd1, fbdl1, fbl1, fblu1,
	fslu1, fslur1, fslr1, fslrd1, fsld1, fsldl1, fsll1, fsllu1,
	fsru1, fsrur1, fsrr1, fsrrd1, fsrd1, fsrdl1, fsrl1, fsrlu1,
	crates, flagB, flagR;

	public static Animation dedup1;
	//	public static Animation  death;
	private static boolean mouselook;

	public Image flag, mag1, pp, as, gilz;

	public Image[] deads;

	public TextField ip;

	private boolean showmap;
	private int timertwo, startInt;

	private Entity targetA;
	private boolean autoaimR;
	private Animation sbgs;
	private Animation trees;
	private Animation grass;
	private int timerAim;
	private Animation city;
	private int stateID;
	private Animation constrX3;
	private Image vremBaz;
	public static boolean managerstate;

	public static Sound akshot, ppshot, ded, ricko, bhit;
	private static boolean deltaget;
	private static long sysStarttiem;

	//	private Pregrada prega;
	//	public static Path rpth;

	public Maino( int stateID)
	{
		this.stateID = stateID;
	}
//	public Maino(String title) {
//		super(title);
//	}

//	public static void main(String[] args) {
//		try {
//			AppGameContainer appgc;
//			appgc = new AppGameContainer(new Maino("Ofiz Wars"));
//			appgc.setDisplayMode(1024, 768, false);
//			//			appgc.setDisplayMode(1920, 1080, true);
//
//			appgc.setMinimumLogicUpdateInterval(15);
//			//			appgc.setMouseGrabbed(true);
//			appgc.setTargetFrameRate(100);
//			appgc.setAlwaysRender(true);
//
//			appgc.start();
//		} catch (SlickException ex) {
//			Logger.getLogger(Maino.class.getName()).log(Level.SEVERE, null, ex);
//		}
//	}

	@Override
	public void init(GameContainer gc, StateBasedGame arg1) throws SlickException {
		gc.setSoundVolume(2);
		ricko = new Sound("snd/rickow.ogg");
		bhit = new Sound("snd/grndimpact.wav");
		akshot = new Sound("snd/akshot.wav");
		ppshot = new Sound("snd/smgshot.wav");
		ded =  new Sound("snd/ded.ogg");
		//		ded = new Sound("snd/akshot.wav")

		gc.getGraphics().setBackground(new Color(76,145,65));

		startInt = 0;

		zoom = 1;

		gcc = gc;

		//		wld = new World();



		timerone = 0;
		ip = new TextField(gc, gc.getDefaultFont(), (gc.getWidth() / 2), 50,
				200, 20);
		ip.setBorderColor(Color.green);
		ip.setBackgroundColor(Color.white);
		ip.setTextColor(Color.black);
		ip.setText("localhost");

		//		int xpl = 4, ypl = 0;


		city = new Animation(new SpriteSheet(new Image("img/CityTest.png", false,
				Image.FILTER_NEAREST), 320, 320), 100);
		grass = new Animation(new SpriteSheet(new Image("img/grass_.png", false,
				Image.FILTER_NEAREST), 64, 64), 100);
		trees = new Animation(new SpriteSheet(new Image("img/trees.png", false,
				Image.FILTER_NEAREST), 64, 128), 100);
		crates = new Animation(new SpriteSheet(new Image("img/crates.png",
				false, Image.FILTER_NEAREST), 16, 16), 100);
		sbgs = new Animation(new SpriteSheet(new Image("img/sbg1.png",
				false, Image.FILTER_NEAREST), 64, 64), 100);
		
		constrX3 = new Animation(new SpriteSheet(new Image("img/constr3.png",
				false, Image.FILTER_NEAREST), 192, 192), 100);
		vremBaz = new Image("img/VremBaza.png",
				false, Image.FILTER_NEAREST);

		// flag
		SpriteSheet fll = new SpriteSheet(new Image("img/flags.png", false,
				Image.FILTER_NEAREST), 16, 16);
		Image[] flR = { fll.getSprite(0, 1), fll.getSprite(1, 1),
				fll.getSprite(2, 1), fll.getSprite(3, 1) };
		flagR = new Animation(flR, 100);

		Image[] flB = { fll.getSprite(0, 2), fll.getSprite(1, 2),
				fll.getSprite(2, 2), fll.getSprite(3, 2) };
		flagB = new Animation(flB, 100);

		flag = fll.getSprite(0, 0);

		mag1 = new Image("img/mag.png", false,
				Image.FILTER_NEAREST);

		gilz = new Image("img/gilz.png", false,
				Image.FILTER_NEAREST);

		pp = new Image("img/pp.png", false,
				Image.FILTER_NEAREST);
		as = new Image("img/as.png", false,
				Image.FILTER_NEAREST);
		//		System.out.println(GL11.glGetInteger(GL11.GL_MAX_TEXTURE_SIZE));
		//TODO --CHARACTER ANIMATIONS
		Animation run = new Animation(new SpriteSheet(new Image("img/Sold1/Run1.PNG", false,
				Image.FILTER_NEAREST), 64, 64),100);
		Animation idle = new Animation(new SpriteSheet(new Image("img/Sold1/Idle1.PNG", false,
				Image.FILTER_NEAREST), 64, 64),100);
		Animation dead = new Animation(new SpriteSheet(new Image("img/Sold1/Dead1.PNG", false,
				Image.FILTER_NEAREST), 100, 76),100);
		Animation ff = new Animation(new SpriteSheet(new Image("img/Sold1/FFire.PNG", false,
				Image.FILTER_NEAREST), 64, 64),100);
		Animation fb = new Animation(new SpriteSheet(new Image("img/Sold1/BFire.PNG", false,
				Image.FILTER_NEAREST), 64, 64),100);
		Animation fsr = new Animation(new SpriteSheet(new Image("img/Sold1/RStrfire.PNG", false,
				Image.FILTER_NEAREST), 64, 64),100);
		Animation fsl = new Animation(new SpriteSheet(new Image("img/Sold1/LStrfire.PNG", false,
				Image.FILTER_NEAREST), 64, 64),100);

		int aspeed = 40;

		runup1 = new Animation(getThree(run, 0, 14), aspeed);
		runrup1 = new Animation(getThree(run, 1, 14), aspeed);
		runright1 = new Animation(getThree(run, 2, 14), aspeed);
		runrdown1 = new Animation(getThree(run, 3, 14), aspeed);
		rundown1 = new Animation(getThree(run, 4, 14), aspeed);
		runldown1 = new Animation(getThree(run, 5, 14), aspeed);
		runleft1 = new Animation(getThree(run, 6, 14), aspeed);
		runlup1 = new Animation(getThree(run, 7, 14), aspeed);

		idleup1 = new Animation(getThree(idle, 0, 16), aspeed);
		idleupr1 = new Animation(getThree(idle, 1, 16), aspeed);
		idler1 = new Animation(getThree(idle, 2, 16), aspeed);
		idlerd1 = new Animation(getThree(idle, 3, 16), aspeed);
		idled1 = new Animation(getThree(idle, 4, 16), aspeed);
		idledl1 = new Animation(getThree(idle, 5, 16), aspeed);
		idlel1 = new Animation(getThree(idle, 6, 16), aspeed);
		idlelu1 = new Animation(getThree(idle, 7, 16), aspeed);

		dedup1 = new Animation(getThree(dead, 0, 23), aspeed);
		dedupr1 = new Animation(getThree(dead, 1, 23), aspeed);
		dedr1 = new Animation(getThree(dead, 2, 23), aspeed);
		dedrd1 = new Animation(getThree(dead, 3, 23), aspeed);
		dedd1 = new Animation(getThree(dead, 4, 23), aspeed);
		deddl1 = new Animation(getThree(dead, 5, 23), aspeed);
		dedl1 = new Animation(getThree(dead, 6, 23), aspeed);
		dedlu1 = new Animation(getThree(dead, 7, 23), aspeed);

		ffu1 = new Animation(getThree(ff, 0, 25), aspeed);
		ffur1= new Animation(getThree(ff, 1, 25), aspeed);
		ffr1 = new Animation(getThree(ff, 2, 25), aspeed);
		ffrd1 = new Animation(getThree(ff, 3, 25), aspeed);
		ffd1 = new Animation(getThree(ff, 4, 25), aspeed);
		ffdl1 = new Animation(getThree(ff, 5, 25), aspeed);
		ffl1 = new Animation(getThree(ff, 6, 25), aspeed);
		fflu1 = new Animation(getThree(ff, 7, 25), aspeed);

		fbu1 = new Animation(getThree(fb, 0, 22), aspeed);
		fbur1 = new Animation(getThree(fb, 1, 22), aspeed);
		fbr1 = new Animation(getThree(fb, 2, 22), aspeed);
		fbrd1 = new Animation(getThree(fb, 3, 22), aspeed);
		fbd1 = new Animation(getThree(fb, 4, 22), aspeed);
		fbdl1 = new Animation(getThree(fb, 5, 22), aspeed);
		fbl1 = new Animation(getThree(fb, 6, 22), aspeed);
		fblu1 = new Animation(getThree(fb, 7, 22), aspeed);

		fslu1 = new Animation(getThree(fsl, 0, 25), aspeed);
		fslur1 = new Animation(getThree(fsl, 1, 25), aspeed);
		fslr1 = new Animation(getThree(fsl, 2, 25), aspeed);
		fslrd1 = new Animation(getThree(fsl, 3, 25), aspeed);
		fsld1 = new Animation(getThree(fsl, 4, 25), aspeed);
		fsldl1 = new Animation(getThree(fsl, 5, 25), aspeed);
		fsll1 = new Animation(getThree(fsl, 6, 25), aspeed);
		fsllu1 = new Animation(getThree(fsl, 7, 25), aspeed);

		fsru1 = new Animation(getThree(fsr, 0, 25), aspeed);
		fsrur1 = new Animation(getThree(fsr, 1, 25), aspeed);
		fsrr1 = new Animation(getThree(fsr, 2, 25), aspeed);
		fsrrd1 = new Animation(getThree(fsr, 3, 25), aspeed);
		fsrd1 = new Animation(getThree(fsr, 4, 25), aspeed);
		fsrdl1 = new Animation(getThree(fsr, 5, 25), aspeed);
		fsrl1 = new Animation(getThree(fsr, 6, 25), aspeed);
		fsrlu1 = new Animation(getThree(fsr, 7, 25), aspeed);




		//		deads = new Image[24];
		//		for (int a = 0; a < 2; a++) 
		//			for (int b = 0; b < 12; b++) 
		//				deads[b+a*12] = charani.getImage(81+b*2+a*200);


		//		//TODs Preg test
		//				prega = new Pregrada(0, 0, 2);
		//				wld.addObj(prega, null, true);

//		System.out.println(" maino inited");

	}

	public static void endGame(int wteam) {
		endgame = true;
		timerone = 0;
		winteam = wteam;
	}



	

	@Override
	public void render(GameContainer gc, StateBasedGame arg1, Graphics g) throws SlickException {

		if (started) {
				if (!debug)
					drawBG(g, gc);


				drawShit(g, gc);
				drawMovables(g, gc);
			
		}
		//		drawHouse(wtd(prega, gc), prega);

		//---INTERFACE

		//		if (client && !connected) {
		//			g.drawString("connect to:", gc.getWidth() / 2, 30);
		//
		//			ip.render(gc, g);
		//
		//			// System.out.println(ip.getText() + ip.getX() + " - " + ip.getY());
		//
		//		}
		//		if (connected || server)
		if (started)
			g.setColor(new Color(0,0,0,0.1f));


		g.drawString("Ofiz Wars!", gc.getWidth() / 2 - 50, 10);

		//		if (server)
		//			g.drawString("Server online!", gc.getWidth() / 2, 30);

		g.setColor(Color.white);

		//		
		//prizel
		if (started) {
			float mx = gc.getInput().getMouseX(), my = gc.getInput().getMouseY();

			int[] target = {(int) (mx+(wld.plr.x-gc.getWidth()/2)), (int) (my+(wld.plr.y-gc.getHeight()/2))};
			if (mouselook) {
				target[0] += (mx - gc.getWidth()/2 );
				target[1] += (my - gc.getHeight()/2 );
			}
			float dist = (float) World.getDist(wld.plr.x+10, target[0], wld.plr.y+10, target[1]);
			if (wld.plr.wpn != null) {
				float razb =(float) wld.plr.wpn.metkst*(1f+wld.plr.wpn.otd);
				if (wld.plr.move) razb *= 2f;

				//			if (razb > wld.plr.wpn.maxRazbr)
				//				razb = wld.plr.wpn.maxRazbr;

				int shir = (int) Math.sqrt((dist*dist)*2 - (2*(dist*dist) * Math.cos(Math.toRadians(razb))));
				g.drawOval(mx-shir/2, my-shir/2, shir, shir);
				if (debug)
					g.drawString(Float.toString(dist), mx-shir/2, my-shir/2);
			}
			g.setColor(Color.black);
			g.fillRect(mx-1, my-1, 3, 3);
		}

		if (debug && aim.asMap != null) {
			if (autoaimR) {
				int[] wtd = wtd(targetA, gc);
				g.setColor(Color.red);
				g.drawOval(wtd[0], wtd[1], 64, 64);
				g.setColor(Color.white);
			}

			for (int a = 0; a < aim.asMap.getSizeX(); a++) 
				for (int b = 0; b < aim.asMap.getSizeY(); b++) {

					int[] wtd = wtd(aim.otnX + (a*World.kvx), aim.otnY + (b*World.kvx), gc);
					if ( aim.asMap.get(a, b) == aim.asMap.WALL ) 
						g.fillRect(wtd[0], wtd[1], World.kvx*zoom, World.kvx*zoom);;
				}
		}


		if (showmap) {
			//gc.getHeight()/20
			int sx = gc.getWidth()/6, sy = 0, pex = gc.getHeight()/(wld.worldbsize);
			for(int x = 0; x < wld.worldbsize; x++)
				for(int y = 0; y < wld.worldbsize; y++) {
					if (wld.mape.mapo[x][y] != null) 
						g.setColor(wld.mape.mapo[x][y]);
					else 
						g.setColor(Color.gray);
					g.fillRect(sx + x*pex, sy+ y*pex, pex, pex);
				}

		}

	}



	private void drawBG(Graphics g, GameContainer gcc) {
		// System.out.println(mapsh.getFrameCount());
		Random bgR = new Random();
		int[] sx ={(int) (wld.plr.x- gcc.getWidth())/ World.kvx, (int) (wld.plr.y - gcc.getHeight())/ World.kvx -1 };
		for (int x = sx[0]; x < sx[0] + gcc.getWidth()/30; x++) {
			for (int y = sx[1]; y < sx[1] + gcc.getHeight()/30; y++) {
				int[] wtd = wtd(x *  World.kvx , y* World.kvx , gcc);

				//				if (!inFocus(wtd[0], wtd[1], gcc)) continue;
				bgR.setSeed(wld.getXYSeed(x, y));
				//				if (bgR.nextBoolean())
				switch (bgR.nextInt(3)) {
				case 0:
					grass.getImage(0).draw(wtd[0], wtd[1], zoom);
					break;

				case 1:
					grass.getImage(1).draw(wtd[0], wtd[1], zoom);
					break;

				case 2:
					grass.getImage(2).draw(wtd[0], wtd[1], zoom);
					break;
				}

				//								if (bgR.nextFloat() <= 0.1)
				//									if (wld.checkObj(x*16,y*16)) 
				//										wld.placeTree(x*16, y*16);

			}
		}
	}

	private void drawZoom(int[] wtd, Animation ani) {
		ani.draw(wtd[0], wtd[1], ani.getWidth()*zoom,  ani.getHeight()*zoom);
	}
	private void drawZoom(int[] wtd, Image img) {
		img.draw(wtd[0], wtd[1], zoom);		
	}

	private static boolean inFocus(float wtdX, float wtdY, GameContainer gcc) {
		if (wtdX >= -321 && wtdX < gcc.getWidth()+321 
				&& wtdY >= -321 && wtdY < gcc.getHeight()+321) return true;
		return false;
	}

	private void drawMovables(Graphics g, GameContainer gcc) {
		ArrayList<Entity> objcs = wld.getObjects();
		for (int a = 0; a < objcs.size(); a++) {
			Entity oba = objcs.get(a);
			//
			if (oba == null ) continue;
			if ( oba.type == 0 || oba.type == 1) {

				int[] wtd = wtd(oba, gcc);

				if (!inFocus(wtd[0], wtd[1], gcc)) continue;

				Fighter plr = (Fighter) oba;

				if (debug) {
					if (oba.type == 1) {
						Enemy ena = (Enemy) oba;
						if (ena.follow) {
							g.setColor(Color.green);
							g.fillRect(wtd[0]+oba.xxWidth, wtd[1]+oba.yyWidth, oba.xWidth, oba.yWidth);
							g.drawString(Integer.toString(ena.id), wtd[0]+20, wtd[1]+oba.yyWidth);
						}
						//						int xxx = , yyy = ena.tgtLoc 
						if (ena.path != null && ena.tgtLoc != null && ena.pfollow) {
							int[] wtd2 = wtd(ena.tgtLoc.getX()* World.kvx + ena.path.otnX,ena.tgtLoc.getY()* World.kvx + ena.path.otnY, gcc);
							g.drawLine(wtd[0], wtd[1], wtd2[0], wtd2[1]);
							g.drawString(ena.path.otnX + "x" + ena.path.otnY, wtd2[0], wtd2[1]);
						}
						// RAY DEBUG
						//						g.setColor(Color.magenta);
						//						ena.drawRay( g, gcc);

						//						g.setColor(Color.magenta);
						//						String mss = Integer.toString((int) (oba.x/16)) + "x" + Integer.toString((int) (oba.y/16));
						//						g.drawString(mss, wtd[0], wtd[1]- 30);
						//						int[] wtdd = wtd((oba.x+ oba.xxWidth) - oba.x%16, (oba.y+ oba.yyWidth) - oba.y %16, gcc);
						//						g.drawRect(wtdd[0] , wtdd[1] , 16, 16);
					}

					//					g.setColor(Color.blue);
					//					g.drawRect(wtd[0]+oba.xxWidth, wtd[1]+oba.yyWidth, oba.xWidth*zoom, oba.yWidth*zoom);
				}

				if (wld.plr.id == plr.id) {
					if (debug)
						g.drawString((int)(plr.x/ World.kvx) + "x" + (int)(plr.y/ World.kvx), wtd[0] - 3, wtd[1] - 35);

					g.setColor(Color.green);
					g.drawString("v",  wtd[0] + oba.xxWidth, wtd[1] - oba.yyWidth);
					g.setColor(Color.white);
					g.drawString(Integer.toString(wld.plr.hp),  wtd[0] + oba.xxWidth, wtd[1] +  World.kvx);
					if (wld.plr.wpn != null) {
						g.drawString(Integer.toString(wld.plr.wpn.ammo),  wtd[0] +  World.kvx, wtd[1]);

						switch	 (wld.plr.wpn.type) {
						case 0 :
							as.draw(gcc.getWidth() / 2-8*5, gcc.getHeight()-50, 5);
							break;
						case 1:
							pp.draw(gcc.getWidth() / 2-8*5, gcc.getHeight()-50, 5);
							break;
						}
					}
				}
				int ttm = 1;
				//PLAYER-----------------
				if (oba.type == 0 || oba.type == 1) {
					Fighter plrx = (Fighter) oba;
					ttm = plrx.team.skin;

				}


				if (plr.move && plr.attMd) 
					drawZoom(wtd, getFireAnim(plr.fdir, plr.mdir));
				else
					if (!plr.move && plr.attMd) 
						drawZoom(wtd, getFireAnim(plr.fdir, plr.fdir).getImage(15));
					else
						if (plr.move) {

							if (plr.fdir >= -0.39 && plr.fdir < 0.39)
								//						switch (ttm) {
								//						case 1:

								drawZoom(wtd, runleft1);
							//						else 
							//							drawZoom(wtd, getFireAnim(plr.fdir, plr.mdir));
							//							//							g.drawAnimation(left1, wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, left2);
							//							//							g.drawAnimation(left2, wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= 0.39 && plr.fdir < 1.17)
								//						switch (ttm) {
								//						case 1:
								drawZoom(wtd, runlup1);

							//							//							g.drawAnimation(lup1, wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, lup2);
							//							//							g.drawAnimation(lup2, wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= 1.17 && plr.fdir < 1.96)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(up1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, runup1);
							//							//							g.drawAnimation(up1, wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, up2);
							//							//							g.drawAnimation(up2, wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= 1.96 && plr.fdir < 2.74)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(rup1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, runrup1);
							//							//							g.drawAnimation(rup1, wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, rup2);
							//							//							g.drawAnimation(rup2, wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= 2.74 || plr.fdir < -2.74)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(up1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, runright1);
							//							//							g.drawAnimation(right1, wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd,right2);
							//							//							g.drawAnimation(right2, wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= -2.74 &&plr.fdir < -1.96 )
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(rdown1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, runrdown1);
							//							//							g.drawAnimation(rdown1, wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, rdown2);
							//							//							g.drawAnimation(rdown2, wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= -1.96 && plr.fdir < -1.17)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(down1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, rundown1);
							//							//							g.drawAnimation(down1, wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd,down2);
							//							//							g.drawAnimation(down2, wtd[0], wtd[1]);
							//							break;
							//						}
							if (plr.fdir >= -1.17 && plr.fdir < -0.39)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(ldown1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, runldown1);
							//							//							g.drawAnimation(ldown1, wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, ldown2);
							//							//							g.drawAnimation(ldown2, wtd[0], wtd[1]);
							//							break;
							//						}

						}
						else {
							/// - STAND----------------------------------
							if (plr.fdir >= -0.39 && plr.fdir < 0.39)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(up1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, idlel1);
							//							//							g.drawImage(left1.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, left2.getImage(1));
							//							//							g.drawImage(left2.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= 0.39 && plr.fdir < 1.17)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(up1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, idlelu1);
							//							//							g.drawImage(lup1.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, lup2.getImage(1));
							//							//							g.drawImage(lup2.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= 1.17 && plr.fdir < 1.96)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(up1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, idleup1);
							//							//							g.drawImage(up1.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, up2.getImage(1));
							//							//							g.drawImage(up2.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						}
							if (plr.fdir >= 1.96 && plr.fdir < 2.74)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(rup1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, idleupr1);
							//							//							g.drawImage(rup1.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, rup2.getImage(1));
							//							//							g.drawImage(rup2.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						}




							if (plr.fdir >= 2.74 || plr.fdir < -2.74)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(up1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, idler1);
							//							//							g.drawImage(right1.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, right2.getImage(1));
							//							//							g.drawImage(right2.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= -2.74 &&plr.fdir < -1.96 )
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(rdown1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, idlerd1);
							//							//							g.drawImage(rdown1.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, rdown2.getImage(1));
							//							//							g.drawImage(rdown2.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						}

							if (plr.fdir >= -1.96 && plr.fdir < -1.17)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(down1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, idled1);
							//							//							g.drawImage(down1.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, down2.getImage(1));
							//							//							g.drawImage(down2.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						}
							if (plr.fdir >= -1.17 && plr.fdir < -0.39)
								//						switch (ttm) {
								//						//					case 0:
								//						//						g.drawAnimation(ldown1, wtd[0], wtd[1]);
								//						//						break;
								//						case 1:
								drawZoom(wtd, idledl1);
							//							//							g.drawImage(ldown1.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						case 2:
							//							drawZoom(wtd, ldown2.getImage(1));
							//							//							g.drawImage(ldown2.getImage(1), wtd[0], wtd[1]);
							//							break;
							//						}
						}
				//				switch (plr.team) {
				//				case 0:
				//					g.drawImage(down1.getImage(1), wtd[0], wtd[1]);
				//					break;
				//				case 1:
				//					g.drawImage(down1.getImage(1), wtd[0], wtd[1]);
				//					break;
				//				case 2:
				//					g.drawImage(down2.getImage(1), wtd[0], wtd[1]);
				//					break;
				//				}


				// g.drawString(Integer.toString(plr.hp), plr.x - 5, plr.y +
				// 16);
				// g.drawString(Integer.toString(plr.id), plr.x - 5, pldr.y -
				// 20);
			}
		}
	}

	private Animation getFireAnim(float fdir, float mdir) {

		float distra = mdir - 1.57f;
		//		System.out.println("fie " + fdir + " mdo " + distra);
		//x
		if (distra >= -0.39 && distra < 0.39)
		{
			//up
			if (fdir >= 0.78 && fdir < 2.34)
				return fslu1;

			//right
			if (fdir >= 2.34 || fdir < -2.34)
				return fbr1;		

			//down
			if (fdir >= -2.34 && fdir < -0.78)
				return fsrd1;

			//left
			if (fdir >= -0.78 && fdir < 0.78)
				return ffl1;

		}
		//			return fsll1;
		//				drawZoom(wtd, runleft1);

		//y
		if (distra >= 0.39 && distra < 1.17)
		{
			//forward
			if (fdir >= 0 && fdir < 1.56)
				return fflu1;

			//right
			if (fdir >= 1.56 )
				return fslur1;		

			//back
			if (fdir <= -1.56 )
				return fbrd1;

			//left
			if (fdir >= -1.56 && fdir < 0)
				return fsrdl1;

		}
		//				drawZoom(wtd, runlup1);
		//			
		//x
		if (distra >= 1.17 && distra < 1.96)
		{
			//forward
			if (fdir >= 0.78 && fdir < 2.34)
				return ffu1;

			//right
			if (fdir >= 2.34 || fdir < -2.34)
				return fslr1;		

			//back
			if (fdir >= -2.34 && fdir < -0.78)
				return fbd1;

			//left
			if (fdir >= -0.78 && fdir < 0.78)
				return fsrl1;

		}	
		//y
		if (distra >= 1.96 && distra < 2.74)
		{
			//left
			if (fdir >= 0 && fdir < 1.56)
				return fsrlu1;

			//forward
			if (fdir >= 1.56 )
				return ffur1;		

			//right
			if (fdir <= -1.56 )
				return fslrd1;

			//back
			if (fdir >= -1.56 && fdir < 0)
				return fbdl1;

		}		
		//				drawZoom(wtd, runrup1);
		//			
		//x
		if (distra >= 2.74 || distra < -2.74)
		{
			//left
			if (fdir >= 0.78 && fdir < 2.34)
				return fsru1;

			//fwd
			if (fdir >= 2.34 || fdir < -2.34)
				return ffr1;		

			//right
			if (fdir >= -2.34 && fdir < -0.78)
				return fsld1;

			//back
			if (fdir >= -0.78 && fdir < 0.78)
				return fbl1;

		}		
		//y
		if (distra >= -2.74 && distra < -1.96 )
		{
			//back
			if (fdir >= 0 && fdir < 1.56)
				return fblu1;

			//left
			if (fdir >= 1.56 )
				return fsrur1;		

			//fwd
			if (fdir <= -1.56 )
				return ffrd1;

			//right
			if (fdir >= -1.56 && fdir < 0)
				return fsldl1;

		}		
		//x
		if (distra >= -1.96 && distra < -1.17)
		{
			//back
			if (fdir >= 0.78 && fdir < 2.34)
				return fbu1;

			//left
			if (fdir >= 2.34 || fdir < -2.34)
				return fsrr1;		

			//fwd
			if (fdir >= -2.34 && fdir < -0.78)
				return ffd1;

			//right
			if (fdir >= -0.78 && fdir < 0.78)
				return fsll1;

		}

		if (distra >= -1.17 && distra < -0.39)
		{
			//right
			if (fdir >= 0 && fdir < 1.56)
				return fsllu1;

			//back
			if (fdir >= 1.56 )
				return fbur1;		

			//left
			if (fdir <= -1.56 )
				return fsrrd1;

			//fwd
			if (fdir >= -1.56 && fdir < 0)
				return ffdl1;

		}
		return dedd1;
	}

	private void drawShit(Graphics g, GameContainer gcc) {
		ArrayList<Entity> objcs = wld.getObjects();
		for (int a = 0; a < objcs.size(); a++) {
			Entity oba = objcs.get(a);

			if (oba == null) continue;

			int[] wtd = wtd(oba, gcc);

			if (!inFocus(wtd[0], wtd[1], gcc)) continue;

			if (debug) {
				g.setColor(Color.lightGray);
				g.drawRect(wtd[0]+oba.xxWidth, wtd[1]+oba.yyWidth, oba.xWidth*zoom, oba.yWidth*zoom);
				//				if (oba.type != 1) {
				//					g.setColor(Color.cyan);
				//					String mss = Integer.toString((int) (oba.x/16)) + "x" + Integer.toString((int) (oba.y/16));
				//					g.drawString(mss, wtd[0], wtd[1]- 30);
				//					int[] wtdd = wtd((oba.x+oba.xxWidth) - oba.x%16, (oba.y+oba.yyWidth) - oba.y %16, gcc);
				//					g.fillRect(wtdd[0] , wtdd[1] , 16, 16);
				//				}
			}

			switch (oba.type) {
			case 0:
				continue;

			case 2:
				int trsz = 15;
				Bullet bl = (Bullet) oba;
				float tox = (float) (wtd[0] - (trsz* Math.sin(bl.rot)*zoom)),
						toy = (float) (wtd[1]+ (trsz*Math.cos(bl.rot)*zoom));
				Color blcrl = bl.faza.trcrclr == 1 ? Color.red : Color.blue;
				g.drawGradientLine(tox, toy, blcrl , wtd[0], wtd[1], Color.black);
				break;

			case 3:
				Pregrada cr = (Pregrada) oba;
				//				crates.getImage(cr.ctype).draw( wtd[0], wtd[1]); crates.getImage(cr.ctype)
				drawZoom(wtd, sbgs);

				break;

			case 4:
				Flag flg = (Flag) oba;
				// g.drawString(Integer.toString(flg.lastTeam), flg.x - 5, flg.y
				// + 16);

				switch (flg.lastTeam) {
				case 0:
					//					flag.draw( wtd[0], wtd[1]);
					drawZoom(wtd, flag);
					break;
				case 1:
					//					flagR.draw( wtd[0], wtd[1]);
					drawZoom(wtd, flagR);
					break;
				case 2:
					//					flagB.draw( wtd[0], wtd[1]);
					drawZoom(wtd, flagB);
					break;
				}
				break;

			case 6:
				//musor
				Musor msr = (Musor) oba;
				int shdvx = 1;
				switch (msr.vid) {
				case 0:
					gilz.setRotation((float) Math.toDegrees(msr.rotdr));
					//					gilz.draw(wtd[0], wtd[1]);
					drawZoom(wtd, gilz);
					//					g.setColor(Color.yellow);
					//					g.fillRect(wtd[0], wtd[1], 3, 2);
					//
					//					g.setColor(Color.white);
					break;
				case 1:
					mag1.setRotation((float) Math.toDegrees(msr.rotdr));
					//					mag1.draw(wtd[0], wtd[1]);
					drawZoom(wtd, mag1);
					break;

				case 2:
				case 3:
					//					msr.ani.draw(wtd[0], wtd[1]);
					if (msr.ani != null) {
						wtd[0] -= msr.ani.getImage(0).getWidth()/2;
						drawZoom(wtd, msr.ani);
					} else {
						//						FIXME Govnokod
						wtd[0] -= dedup1.getImage(0).getWidth()/2;
						drawZoom(wtd, dedup1.getImage(0));
					}
					shdvx = 3;
					break;
				}
				if (msr.vid >= 200 && msr.vid < 300) { 
					wtd[0] -= dedup1.getImage(22).getWidth()/2;
					drawZoom(wtd, dedup1.getImage(22));
				}
				//					deads[msr.vid-200].draw(wtd[0], wtd[1]);
				if (msr.vid >= 300)
					drawZoom(wtd, deads[msr.vid-288]);
				//					deads[msr.vid-288].draw(wtd[0], wtd[1]);
				//				System.out.println(msr.vid);
				if (msr.z > 0) {
					g.setColor(new Color(0,0,0,0.1f));
					g.fillOval(wtd[0], wtd[1]+msr.z, 3*shdvx*zoom, 2*shdvx*zoom);

				} else msr.transform();
				break;
			case 7:
				//trees
				Pregrada preg = (Pregrada) oba;

				switch (preg.ctype) {
				case 10:
					trees.getImage(0).draw(wtd[0], wtd[1], zoom);
					break;
				case 11:
					trees.getImage(1).draw(wtd[0], wtd[1], zoom);
					break;
				case 12:
					trees.getImage(2).draw(wtd[0], wtd[1], zoom);
					break;
					//				case 13:
					//					mapsh.getImage(441).draw(wtd[0], wtd[1]+16*zoom, zoom);
					//					mapsh.getImage(404).draw(wtd[0], wtd[1], zoom);
					//
					//					break;	
					//				case 14:
					//					mapsh.getImage(442).draw(wtd[0], wtd[1]+16*zoom, zoom);
					//					mapsh.getImage(405).draw(wtd[0], wtd[1], zoom);
					//
					//					break;
					//				case 15:
					//					mapsh.getImage(443).draw(wtd[0], wtd[1]+16*zoom, zoom);
					//					mapsh.getImage(406).draw(wtd[0], wtd[1], zoom);
					//
					//					break;
				}


				break;

			case 8:
			case 9:
				Pregrada prega = (Pregrada) oba;
				drawHouse(wtd, prega);
				break;
				
			case 201:
				vremBaz.draw(wtd[0], wtd[1], zoom);
				if (debug) {
					Facility vb = (Facility) oba;
					int[] xy;
					for (int as = 0; as < vb.spawns.size(); as++) {
						xy = wtd(vb.spawns.get(as).x, vb.spawns.get(as).y, gcc);
						g.drawString("RESP", xy[0], xy[1]);
					}
					g.drawString(Integer.toString(vb.garizon), wtd[0], wtd[1]);
					
				}
				break;
				
			case 2201:
				//constr 3x3
				constrX3.getImage(0).draw(wtd[0], wtd[1], zoom);
				break;


			}
		}
	}
	//TODO houses
	private void drawHouse(int[] wtd, Pregrada preg) {
		switch (preg.ctype) {
		case 90:
			city.getImage(3).draw(wtd[0], wtd[1], zoom);
			break;
		case 91:
			//+
			city.getImage(4).draw(wtd[0], wtd[1], zoom);
			break;
		case 92:
			//-
			city.getImage(5).draw(wtd[0], wtd[1], zoom);
			break;

		case 100:
			//dom1 layer1
			city.getImage(0).draw(wtd[0], wtd[1], zoom);
			break;
		case 101:
			city.getImage(1).draw(wtd[0], wtd[1], zoom);
			break;
		case 102:
			city.getImage(2).draw(wtd[0], wtd[1], zoom);
			break;
		}
	}

	private int dhx(int step) {
		return (int) ( World.kvx*step*zoom);
	}

	/**
	 * 
	 * @param charani
	 * @param a
	 * @return
	 */
	private Image[] getThree(Animation charani, int line, int frames) {
		//		int za = a*3;
		Image[] frms = new Image[frames];
		//			{ charani.getImage(a), charani.getImage(a+1), charani.getImage(a+2) };
		for (int a = 0; a < frames; a ++)
			frms[a] = charani.getImage((line*frames) + a);

		return frms;
	}

	public void reloed() {
		wld.plr.reload();
	}

	public void autoaim(GameContainer gc) {
		int autoaimReach = 64;

		int mx = gc.getInput().getMouseX(), my = gc.getInput().getMouseY();
		if (mouselook) {
			mx += (mx - gc.getWidth()/2 );
			my += (my - gc.getHeight()/2 );
		}

		mx += (wld.plr.x-gc.getWidth()/2);
		my += (wld.plr.y-gc.getHeight()/2);

		targetA = new Entity();
		targetA.x = mx-autoaimReach/2;
		targetA.y = my-autoaimReach/2;

		for (int a = 0; a < wld.ais.size(); a++) {
			Enemy kk = wld.ais.get(a);

			if (kk.team != wld.plr.team && World.getDist(targetA, kk) < autoaimReach	) {
				//				System.out.println("autoaim sss");

				targetA = kk;
				autoaimR = true;
				return;
			}
		}
		autoaimR = false;
	}

	public void fire(int tx, int ty, GameContainer gc, int delta) {
		if (!started) return;

		wld.plr.fdir = (float) (Math.atan2((gc.getHeight()/2-ty), (gc.getWidth()/2-tx)) );

		int[] target = {(int) (tx+(wld.plr.x-gc.getWidth()/2)), (int) (ty+(wld.plr.y-gc.getHeight()/2))};
		//		if (

		//		int[] target = {(int) (mx+(wld.plr.x-gc.getWidth()/2)), (int) (my+(wld.plr.y-gc.getHeight()/2))};
		if (mouselook && !autoaimR) {
			target[0] += (tx - gc.getWidth()/2 );
			target[1] += (ty - gc.getHeight()/2 );
		}
		if (autoaimR) {
			target[0] = (int) targetA.x+targetA.xxWidth;
			target[1] = (int) targetA.y+targetA.yyWidth+16;
		}
		wld.plr.fire(delta, target);
		//				) 
		//		{
		//
		//			Musor mus = new Musor(wld.plr, 0);
		//			Bullet blt = new Bullet(wld.plr.wpn, tx, ty, gc);
		//
		//			if (client && connected) {
		//
		//				DataPok send2 = new DataPok();
		//				send2.type = 5;
		//				send2.ent = mus;
		//				DataPok send = new DataPok();
		//				send.type = 5;
		//				send.ent = blt;
		//
		//				clit.sendTCP(send);
		//				clit.sendTCP(send2);
		//
		//			}
		//
		//			if (server) {
		//				wld.addObj(mus, null, true);
		//				wld.addObj(blt, null, true);
		//			}
		//		}

	}
	public static int[] wtd(float x, float y, float otnX, float otnY, GameContainer gcc) {
		int[] wtd = { (int) (((x - otnX)*zoom + (gcc.getWidth() / 2))),
				(int) (((y - otnY)*zoom + (gcc.getHeight() / 2))) };
		return wtd;
	}

	public static int[] wtd(float x, float y, GameContainer gcc) {
		float mx = 0, my = 0;
		if (mouselook) {
			mx = gcc.getInput().getMouseX() - gcc.getWidth() / 2;
			my = gcc.getInput().getMouseY() - gcc.getHeight() / 2;
		}
		int[] wtd = { (int) (((x - wld.plr.x )*zoom + (gcc.getWidth() / 2)) - ( mx ) ),
				(int) (((y - wld.plr.y )*zoom + (gcc.getHeight() / 2)) - ( my )) };
		return wtd;
	}

	public static int[] wtd(Entity obj, Entity otnObj, GameContainer gcc) {
		return wtd(obj.x, obj.y-obj.z, otnObj.x, otnObj.y, gcc);
	}

	public static int[] wtd(Entity obj, GameContainer gcc) {
		return wtd(obj.x, obj.y-obj.z, gcc);
	}

	public static synchronized void playSound(float x, float y, int sound) {
		int[] wtd = wtd(x, y, gcc);
		wtd[0] -= (gcc.getWidth()/2)*zoom;
		wtd[1] -= (gcc.getHeight()/2)*zoom;

		//		System.out.println("playsound " + World.getDist(0, wtd[0], 0, wtd[1]));

		if (World.getDist(0, wtd[0], 0, wtd[1]) > 1200)
			return;

		float pitch = (float) (1.1 - (Math.random()*0.3));
		//		if (inFocus(wtd[0], wtd[1], gcc)) 
		switch (sound ) {
		case 0:
			//akshot
			akshot.playAt(pitch, 2.5f, wtd[0], wtd[1], 0);
			break;
		case 1:
			//ppshot
			ppshot.playAt(pitch, 1.2f, wtd[0], wtd[1], 0);
			break;
		case 2:
			//ded
			ded.playAt(pitch, 2f, wtd[0], wtd[1], 0);
			break;
		case 3:
			//ricko
			ricko.playAt(pitch, 1f, wtd[0], wtd[1], 0);
			break;
		case 4:
			//bulhit
			bhit.playAt(pitch, 0.1f, wtd[0], wtd[1], 0);
			break;

		}

	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException {
		// wld.remQue(); new Color(88,114,70)
		timerone += delta;
		timertwo += delta;
		timerAim += delta;

		//		if (server) {
		if (started) {
			wld.checkGame(delta);
			//			aim.manage(delta);
			////			if (timerAim >= 20) {
			//			if (delta > 0)
			//							aim.go(delta);
			////				timerAim = 0;
			////			}
			aim.manage(delta);

			//			if (wld.plr.tcity != null) {ыв
			//				Facility fec = (Facility) wld.addCObj(wld.plr.tcity);
			//				System.out.println(" tsity " + wld.plr.tcity.x + "x" + wld.plr.tcity.y);
			//				if (fec != null)
			//					System.out.println(" tsity " + fec.spawns.size() + " wps " + fec.waypoints.size());
			//			}

		}
		else {
			if (MainMenu.isLoading()) 
				loadGame();
			else 
				startNewGame();
		}
		//		if (client && connected) {
		//			wld.chkMusor(delta);
		//		}



		if (timerone >= 500) {
			if (started)
				autoaim(gc);

			timerone = 0;
		}

		if (timertwo >= 1000) {
			if (started) {
				wld.generateOkrestn();

				wld.placeCObj();

				wld.generateCities(150, 0);

				wld.deleteFar(16000);
				aim.makeASMap();
			}

			if (showmap)
				wld.mape.refresh( );
			// // System.out.println("send tcp second");
			// // Poked request = new Poked(2);
			// // clit.sendTCP(request);
			timertwo = 0;
		}
		//		if (endgame) {
		//			if (timerone > 3000)
		//				endgame = false;
		//		}
		// }

		// TODO Controls

		Input input = gc.getInput();
		if (started) {
			if (managerstate) {
				input.clearKeyPressedRecord();
				managerstate = false;
			}
			//			System.out.println(wld.objcs.size());
			wld.plr.move = false;

			if (wld.plr.wpn != null)
				wld.plr.wpn.reload(delta);


			//			if (input.isKeyDown(Input.KEY_K)) {
			//				aim.addBot(wld.fracs.get(1));
			//			}

			if (input.isKeyDown(Input.KEY_A)) {
				wld.plr.movedir(0);
			}
			if (input.isKeyDown(Input.KEY_D)) {
				wld.plr.movedir(2);
			}
			if (input.isKeyDown(Input.KEY_W)) {
				wld.plr.movedir(1);
			}
			if (input.isKeyDown(Input.KEY_S)) {
				wld.plr.movedir(3);
			}
			if (wld.plr != null) {
				wld.plr.setMDir();
				wld.plr.movebyrot(delta);
			}

			if (input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
				//			 System.out.println("pew");
				fire(input.getMouseX(), input.getMouseY(), gc, delta);

			}

			if (input.isKeyDown(Input.KEY_R)) {
				reloed();
			}

			if (input.isKeyPressed(Input.KEY_CAPITAL)) {
				mouselook = !mouselook;
			}
			
			if (input.isKeyPressed(Input.KEY_C)) {
				System.out.println("constr prestd");
				wld.deployLager();
			}
			//			if (input.isKeyDown(Input.KEY_9)) {
			//				wld.plr.team = 1;
			//			}
			//			if (input.isKeyDown(Input.KEY_0)) {
			//				wld.plr.team = 2;
			//			}
			// move
			// if (wld.plr.move) {


			//			DataPok mova = new DataPok();
			//			mova.ent = wld.plr;
			//			mova.type = 4;
			//			if (client && connected)
			//				clit.sendTCP(mova);
			//			if (server)
			//				serv.broadcast(mova);

			if (input.isKeyDown(Input.KEY_EQUALS)) {
				zoom += 0.1f/1000f*delta;
			}
			if (input.isKeyDown(Input.KEY_MINUS)) {
				zoom -= 0.1f/1000f*delta;
			}
			if (input.isKeyDown(Input.KEY_BACK)) {
				zoom = 1;
			}
			
			if ( input.isKeyPressed(Input.KEY_M) ){
				wld.mape.MakeMap(wld);
				showmap = !showmap;
			}
			if ( input.isKeyPressed(Input.KEY_N) ){
				managerstate = true;
				game.enterState(Main.MANAGERSTATE);
				
			}

			if (input.isKeyDown(Input.KEY_1)) {
				changeWeapon(0);
			}
			if (input.isKeyDown(Input.KEY_2)) {
				changeWeapon(1);
			}

		}
		

		// }

		//		if (input.isKeyDown(Input.KEY_O) && !started) {
		//			//				!server && !client) {
		//			//			try {
		//			//				serv = new Servor();
		//			//				server = true;
		//			started = true;
		//
		//			//			wld.newWorld();
		//			aim = new AiManager(wld);
		//
		//		} 
		//		catch (IOException e) {
		//				e.printStackTrace();
		//			}
		//		}

		//		if (input.isKeyPressed(Input.KEY_P) && !server) {
		//
		//			if (client && !connected) {
		//
		//				clit = new Cleend(ip.getText());
		//				// connected = clit.con;
		//				// Maino.wld.removePlr();
		//
		//			}
		//
		//			if (!client)
		//				client = true;
		//
		//		}



		if (input.isKeyPressed(Input.KEY_ESCAPE)) {
			if (started)
				saveGame();
			gc.exit();
		}

		if ( input.isKeyPressed(Input.KEY_L) ){

			debugsprite = !debugsprite;
		}

		if ( input.isKeyPressed(Input.KEY_B) ){
			debug = !debug;

			//			 rpth = 
//			aim.getPathto(wld.plr, wld.plr.x+100, wld.plr.y+100);
			//			System.out.println(" add bot team 1 ");
			//			aim.addBot(1);server &&
		}
		if (input.isKeyPressed(Input.KEY_NUMPAD0)) {

		}

	}

	private void loadGame() {
//		System.out.println(savesList[numer]);

		wld = FileManager.loadObject(MainMenu.getList()[MainMenu.getLNum()]);
		wld.initLoaded();

		aim = new AiManager(wld);
		//		aim.start();

		started = true;
	}

	

	public void saveGame() {
		FileManager.SaveObject(wld);
	}

	private void startNewGame() {
		started = true;
		wld = new World(true);
		wld.init();
		aim = new AiManager(wld);

		//		wld.placeCObj();

		wld.generateOkrestn();

		wld.generateBase(-256 , -256, wld.plr.team);

		wld.generateCities(700, 0);	
		
		wld.deleteFar(0);


		//		aim.start();


	}

	public static void getDelta(boolean nano) {
		if (!deltaget) {
			sysStarttiem = nano ? System.nanoTime() : System.currentTimeMillis();
			deltaget = true;
		} else {
			deltaget = false;
			long tim = (nano ? System.nanoTime() : System.currentTimeMillis() - sysStarttiem);
			if (tim > 0)
				System.out.println("----------------------GET DELTA = " +  tim );
		}
	}

	public void changeWeapon(int num) {
		wld.plr.deswpn = num;
	}


	@Override
	public int getID() {
		return stateID;
	}
}
