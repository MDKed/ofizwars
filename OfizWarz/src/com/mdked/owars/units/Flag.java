package com.mdked.owars.units;

public class Flag extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4676312760464249416L;
	public float progres;
	public int lastTeam;

	public Flag() {
		type = 4;

	}

	public Flag(int x, int y) {
		type = 4;

		this.x = x;
		this.y = y;
	}

	public Flag(int x, int y, int skin) {
		this(x,y);
		lastTeam = skin;
	}

}
