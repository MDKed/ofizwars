package com.mdked.owars.units;

import org.newdawn.slick.Animation;

import com.mdked.owars.core.Maino;

/**
 * 0 - gilza
 * 1 - mag
 * 2,3 200-312 - ded
 * @author operator-7
 *
 */
public class Musor extends Moveable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6805495406698826287L;
	public int vid, lifespan;
	public float rot, zup, rotdr;
	public transient Animation ani;

	public Musor() {}

	public Musor(Entity ent, int type) {
		makeMus(ent.x, ent.y, type);
	}

//	public Musor(int ownerId, int type) {
//		Entity wh = Maino.wld.getIdobj(ownerId);
//		if (wh != null)
//			makeMus(wh.x, wh.y, type);
//	}

	public Musor(Entity ent, int type, Animation an) {
		makeMus(ent.x, ent.y, type);
		ani = an.copy();
	}

	public Musor(int x, int y, int type) {
		makeMus(x, y, type);
	}

	public void makeMus(float x, float y, int type) {
		this.x = x+32;
		this.y = y+20;
		this.type = 6;

		zup = 160;

		vid = type;
		lifespan = 0;
		int xx =0;
		
		rotdr = (float) (Math.random()* Math.toRadians(360));

		rot = (float) (Math.random()*6f);

		switch (type) {
		case 0:
			z = 8;
			break;
		case 1:
			z = 6;
			break;

		case 2:
			//t1
		case 3:
			//t2
			z = 1;
			zup = 500;
			break;
			//			trypiki
		case 200:
			xx = 100;
		case 300:
			vid = (int) (300-xx + Math.random()*12);
			z = 1;
			zup = 100;
			break;
		}
	}

	public void setAnim(Animation an) {
		ani = an.copy();
		ani.setLooping(false);
	}

	public void transform() {
		switch (vid) {
		case 2:
		case 3:
			//			System.out.println("transform from " + vid);

			vid = (int) (vid*100 + Math.random()*12);
			//			System.out.println("transform to " + vid);


			lifespan = 0;
			z = 1;
			zup = 100;
			break;
		}
	}

	public void mova(int delta) {
		if (Maino.wld.checkOColl(this)) {
		x += (45*Math.random() * Math.sin(rot))* delta/1000f;
		y -= (45*Math.random() * Math.cos(rot))* delta/1000f;
		} else {
			x -= (45*Math.random() * Math.sin(rot))* delta/1000f;
			y += (45*Math.random() * Math.cos(rot))* delta/1000f;
		}
		z += 4f * ((zup - lifespan)/zup);
		rotdr += (float) (Math.random()* ((Math.toRadians(360)/200f) * delta));
		
	}

	public void updato(Musor bula) {
		this.x = bula.x;
		this.y = bula.y;
		this.z = bula.z;
	}

}
