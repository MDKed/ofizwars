package com.mdked.owars.units;

import java.util.Random;

import com.mdked.owars.core.Maino;
import com.mdked.owars.core.World;

/**
 *  typo = 0 - crate
 *  1 - tree;
 * @author operator-7
 *
 */
public class Pregrada extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7610321436901423340L;
	public int ctype, penrate, prozukr;
	public boolean ukrit;

	public Pregrada() {}

	public Pregrada(int x, int y, int typo) {
		type = 3;
		this.x = x;
		this.y = y;

		xWidth = World.kvx;
		yWidth = World.kvx;


		switch (typo) {
		case 0:
			randomcr();
			break;
		case 1:
			randomtree();
			break;
		case 2:
			randomhouse();
			break;
		case 90:
		case 91:
		case 92:
			makeroad(typo);
			break;
		}


	}
	
	private void makeroad(int typ) {
		type = 9;
		xWidth = 320;
		yWidth = 320;
//		cityshit = true;
		ctype = typ;
		
	}


	private void randomhouse() {
		Random rnd = new Random(Maino.wld.getXYSeed((int) x,(int)  y));
		
		type = 8;
		ctype = 100 + rnd.nextInt(3);
		xxWidth = 0;
		yyWidth = 0;
		yWidth = 320;
		xWidth = 320;
		prozukr = 95; 
		penrate = 500;

	}

	public void randomcr() {
		Random rnd = new Random(Maino.wld.getXYSeed((int) x,(int)  y));
		
		ctype = rnd.nextInt(4);
		prozukr = 50;
		penrate = 10;
		ukrit = true;
	}

	public void randomtree() {
		Random rnd = new Random(Maino.wld.getXYSeed((int) x,(int)  y));

		type = 7;
		penrate = 15;
		prozukr = 30; 
		ukrit = true;
		yWidth = 128;
		xWidth = 64;
		//		if (!wld.chObjPlace(this)) return;
		ctype = 10+ rnd.nextInt(3);

		//		Pregrada top = new Pregrada();
		//		top.x = this.x;
		//		top.y = this.y-16;
		//		top.ctype = ctype+10;
		//		
		//		wld.addObj(top, null, true);
	}

}
