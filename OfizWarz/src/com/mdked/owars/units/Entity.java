package com.mdked.owars.units;

import java.io.Serializable;

import com.mdked.owars.core.Maino;

/**
 *  0 - player;
 *  1 - enemy;
 *  2 - bullet;
 *  3 - crate;
 *  4 - flag;
 *  5 - respawn;
 *  6 - musor;
 *  7 - tree;
 *  8- house;
 *  9 - road;
 *  100 - city;
 *  200 - base;
 *  201 - lager' resp
 *  300 - (constructions) lager' resp;
 * @author operator-7
 *
 */
public class Entity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5064462642474166666L;
	public float x,y,z;
	public int id, type;
	public int xWidth, yWidth, zWidth, xxWidth, yyWidth, zzWidth;
//	public boolean pust;
	
	public Entity(){
		id = ++Maino.wld.ids;
	};
	public Entity(float x, float y) {
		this();
		this.x = x;
		this.y = y;
	}
}
