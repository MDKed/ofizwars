package com.mdked.owars.units;

import java.util.ArrayList;

import com.mdked.owars.core.Maino;

public class Bullet extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -424470295783358936L;
	public int damage, lifetime;
	public Weapon faza;
	public float rot, speed;
	public boolean hit;

	public ArrayList<Entity> pregs;

	public Bullet(){};

	public Bullet(Weapon fz, float tx, float ty) {
		
		xWidth = 4;
		yWidth = 4;

//		if (Maino.server) 
			pregs = new ArrayList<>();

			type = 2;
			faza = fz;
			//		System.out.println(fz + " id = " + fz.owner);
			Fighter moa = (Fighter) Maino.wld.getIdO(faza.owner);
			//		if (moa == null) return;

			x = moa.x+32;
			y = moa.y+32;

			damage = fz.dmg;

			speed = 520f;
			float razb = (float) (fz.metkst/2 - ((fz.metkst)*Math.random()))*(1f+fz.otd);
			if (moa.move) razb *= 2f;
			//		float razb = 0;
			//		Entity org = Maino.wld.getIdO(fz.owner);
			//Maino.gcc.getWidth()/2
			//		if (razb < -fz.maxRazbr/2) 
			//			razb = -fz.maxRazbr/2;
			//		if (razb > fz.maxRazbr/2)
			//			razb = fz.maxRazbr/2;

			rot = (float) (Math.atan2((y-ty), (x-tx)) - Math.toRadians(90 + razb) );
			moveRot(75);

	}

	public void updato(Bullet bula) {
		rot = bula.rot;
		speed = bula.speed;
		x = bula.x;
		y = bula.y;
	}

	//	public void draw(Graphics g) {
	//		float tox = (float) (x + (15* Math.sin(rot))),
	//				toy = (float) (y- (15*Math.cos(rot)));
	//		g.drawGradientLine(tox, toy, Color.red , x, y, Color.black);
	////		g.drawString(Integer.toString(id), x, y-10);
	//	}

	public void moveRot(int delta) {
		x += (speed * Math.sin(rot))* delta/1000f;
		y -= (speed * Math.cos(rot))* delta/1000f;
		lifetime += delta;
	}

	public float getDmgx() {
		return (float) (faza.distance- lifetime)/(faza.distance/1.35f);
	}

	public boolean chekpreg(Pregrada pregrad) {
		if (pregs.contains(pregrad))
			return false;
		else return true;
	}
}
