package com.mdked.owars.units;

import com.mdked.owars.units.strat.Facility;

public class Respawn extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7462313642418139116L;
//	public Fraction team;
	public Facility origFac;
	
	public Respawn() {}
	public Respawn(int x, int y) {
		type = 5;
		this.x = x;
		this.y = y;
//		this.team = team;
	}
	public Respawn(int x, int y, Facility fac) {
		this(x,y);
		origFac = fac;
	}
	
}
