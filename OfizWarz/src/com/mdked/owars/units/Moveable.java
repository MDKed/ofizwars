package com.mdked.owars.units;

import com.mdked.owars.core.Maino;

public class Moveable extends Entity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5820258753517256107L;
	public float speed, speedMx, lastX, lastY,mdir, fdir, lfx, lfy;
	public int dx, dy;
	public boolean move, frozen, fd;
	
//	public int team;
//	public int hp;
	
	public Moveable() {}

	public void movedir(int dir) {
		//		if (frozen) return;

		//		lastX = x;
		//		lastY = y;
		//
		//		this.dir = dir;
		move = true;
		switch (dir) {
		case 0:
			dx = 1;
			break;
		case 1:
			dy = 1;
			break;
		case 2:
			dx = -1;
			break;
		case 3:
			//			y+=speed*delta;
			dy = -1;
			break;
		}


		//		if (!Maino.wld.checkOColl(this)) {
		//			x = lastX;
		//			y = lastY;
		//		}

	}

	public void movebyrot(int delta) {
		
		
		if (frozen) return;
		if (!move) return;
//		System.out.println("move " + this);
		lastX = x;
		lastY = y;

		y += (speed * Math.cos(mdir))* delta/1000f;
		
		if (!Maino.wld.checkOColl(this)) 
//			y -= (speed * Math.cos(mdir))* delta/1000f;
		y = lastY;
		
		x -= (speed * Math.sin(mdir))* delta/1000f;
		
		if (!Maino.wld.checkOColl(this)) 
			x = lastX;
//			x += (speed * Math.sin(mdir))* delta/1000f;

		
		
		//		if (!Maino.wld.checkOColl(this)) 
		//			x = lastX;
		//			y = lastY;
		//		}
		
		dx = 0;
		dy = 0;
	}

	public void setMDir() {

		if (move) {
//			System.out.println("frozen ");
			fdir =  (float) (Math.atan2((dy), (dx)) );
			mdir = (float) (fdir  + Math.toRadians(90) );
//			System.out.println("mdir " + mdir);
		} 


		//		if (!fd){
		//			lfx = x;
		//			lfy = y;
		//			fd = true;
		//		} else {
		//			if (move)
		//				fdir =  (float) (Math.atan2((lfy-y), (lfx-x)));
		//			fd = false;
		//		}
	}
}
