package com.mdked.owars.units;

import java.io.Serializable;
import java.util.ArrayList;

public class Squad  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2973799392708954978L;

	public Fighter leader;

	public ArrayList<Fighter> roster;

	public Squad() {

	}

	public Squad(Fighter ledor) {
		leader = ledor;
		roster = new ArrayList<>();
	}

	public void addFighter(Enemy gir) {
		roster.add(gir);
	}

	public void remFighter(Fighter fat) {
		for (int a = 0; a < roster.size(); a ++) {
			if (roster.get(a).id == fat.id) {
			
//				System.out.println("Change follow");

				if (a > 0 && a+1 < roster.size()) {
					Enemy fe = (Enemy) roster.get(a+1);
					fe.ftgt = roster.get(a-1);
				}
				else if (a+1 < roster.size()) {
					Enemy fe = (Enemy) roster.get(a+1);
					fe.ftgt = leader;
				}
				roster.remove(a);
				break;
			}
		}
	}

	public void refreshFtgts(Fighter fito) {
		leader = fito;
		for (int a = 0; a < roster.size(); a++)
		{
			Enemy ee = (Enemy) roster.get(a);
			ee.refreshFtgt();
		}
	}

	public Fighter getLastSoldir() {
		if (roster.size() > 0) {
			Fighter buf = roster.get(0);
			//			leader = buf;
			//			Maino.wld.remObj(buf.id);

			//			remFighter(buf);

			return buf;
		}

		return null;
	}

}
