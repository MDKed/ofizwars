package com.mdked.owars.units;

import java.io.Serializable;

import com.mdked.owars.core.Maino;

public class Weapon implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3557975428647168902L;
	public int type, dmg, mindmg, ammo, ammomax, aspd, rspeed, cd, distance, 
	penetr, maxRazbr, trcrclr;
	public float otd, otdps, metkst;
	public boolean reload, ready;

	public int owner;

	public Weapon() {}

	public Weapon(int type, int fazaId, int tracerColor) {

		owner = fazaId;
		
		 trcrclr = tracerColor;

		this.type = type;
		cd = 0;
		otd = 0;
		ready = true;
		maxRazbr = 7;
		switch (type) {
		case 0:
			//aRifle
			mindmg = 15;
			dmg = 25;
			
			ammo = 30;
			ammomax = 30;
			aspd = 112;
			rspeed = 3000;
			penetr = 25;
			otdps = 0.3f;
			distance = 12000;
			metkst = 3;
			break;

		case 1:
			//subMGun
			dmg = 20;
			mindmg = 10;
			ammo = 30;
			ammomax = 30;
			aspd = 75;
			rspeed = 2500;
			penetr = 10;
			otdps = 0.1f;
			distance = 5600;
			metkst = 8;
			break;

		}
	}

	public boolean fire(int delta, int tx, int ty) {
		Entity wh = Maino.wld.getIdO(owner);
		if (wh == null) return false;  
		
		if (ammo > 0) {
			if (ready) {
				ready = false;
				ammo--;
				otd += otdps;

				
				Musor mus = new Musor(wh, 0);

//								System.out.println(" shoot weapon id = " + owner);

				
				
				Bullet blt = new Bullet(this, tx, ty);

//				if (Maino.client && Maino.connected) {
//
//					DataPok send2 = new DataPok();
//					send2.type = 5;
//					send2.ent = mus;
//					DataPok send = new DataPok();
//					send.type = 5;
//					send.ent = blt;
//
//					Maino.clit.sendTCP(send);
//					Maino.clit.sendTCP(send2);
//
//				}

//				if (Maino.server) {
//				System.out.println(" foer");
					Maino.wld.addObj(mus,  true, false, false);
					Maino.wld.addObj(blt,  true, false, false);
					
					Maino.playSound(blt.x, blt.y, type);
//				}
				return true;
			}
		} 
		else 
			reload();
		return false;

	}

	public void reload() {
		if (!reload) {
			reload = true;
			ready = false;
			ammo = 0;
			if (Maino.wld != null) {

				Entity wh = Maino.wld.getIdO(owner);
				if (wh == null) return;
				Musor mus = new Musor(wh, 1);

//				if (Maino.client && Maino.connected) {
//					DataPok send = new DataPok();
//					send.type = 5;
//					send.ent = mus;
//					Maino.clit.sendTCP(send);
//				}
//				if (Maino.server)
					Maino.wld.addObj(mus, true, false, false);
			}
		}

	}
	public void reload(int delta) {
		if (otd > 0) {
			//			System.out.println(" otd= " + otd);
			otd -= otdps*(delta/200f);
		} else otd = 0;
		if (reload || !ready)
			cd += delta;

		if (!reload && !ready && cd > aspd) {
			ready = true;
			cd -= aspd;
		}

		if (reload && cd > rspeed) {
			cd -= rspeed;
			ammo = ammomax;
			reload = false;
			ready = true;
		}
	}
}
