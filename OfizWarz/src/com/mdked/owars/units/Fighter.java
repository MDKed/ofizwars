package com.mdked.owars.units;

import com.mdked.owars.core.Maino;
import com.mdked.owars.core.werld.Fraction;

public class Fighter extends Moveable  {
/**
	 * 
	 */
	private static final long serialVersionUID = -4942017486125513901L;
	//	public int team;
	public Fraction team;
	public int hp, attTimer;
	public float stuntime, stuntimer;
	public int score, deswpn;
	
	public Squad sqd;
	
	public boolean attMd;

	public Weapon wpn;

	public Fighter() {
		hp = 100;
		speed = 80;
		speedMx = speed;
	}

	public void giveWeapon(int weapon) {
		//		System.out.println(" give weapon id = " + id);
		wpn = new Weapon(weapon, id, team.id);
	}

	public boolean fire(int delta, int[] target) {
		return fire(delta, target[0], target[1]);
	}

	protected boolean fire(int delta, int x, int y) {
		attTimer = 0;
		attMd = true;
		return wpn.fire(delta, x, y);

	}
	public void fActivity(int delta) {
		if (attMd) {
			speed = speedMx/2;
			attTimer += delta;
			if (attTimer >= 300) 
			{
				speed = speedMx;
				attTimer = 0;
				attMd = false;
			}
		}
	}

	public void die() {
		
		Musor mus = new Musor(this, 2);
		Maino.playSound(x, y, 2);
		Maino.wld.addObj(mus,  true, false, false);
	}
}
