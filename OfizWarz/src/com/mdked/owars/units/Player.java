package com.mdked.owars.units;

import com.mdked.owars.core.werld.Fraction;
import com.mdked.owars.units.strat.Facility;

public class Player extends Fighter {
	/**
	 * 
	 */
	private static final long serialVersionUID = -603444344932792346L;
	//	private Animation left, up, down; 
//	public int connId;
//	public Facility tcity;





	public Player() {}

	public Player(Fraction team) {
		makePlayer(0,0,team);
	}


	public void makePlayer(int x, int y, Fraction team) {
		type = 0;
		hp = 100;

		this.x = x;
		this.y = y;
		xxWidth = 26;
		yyWidth = 11;
		xWidth = 12;
		yWidth = 35;

		this.team = team;

		speed = 80f;
		speedMx = speed;

		sqd = new Squad(this);
		giveWeapon(0);
		//		wpn = new Weapon((int) (Math.random()*2), id);
	}



	public Player(int x, int y, Fraction team) {
		makePlayer( x,  y,  team); 
	}

	public void takeBody(Fighter fito) {
		id = fito.id;
		type = 0;
		
		x = fito.x;
		y = fito.y;
		xxWidth = fito.xxWidth;
		yyWidth = fito.yyWidth;
		xWidth = fito.xWidth;
		yWidth = fito.yWidth;
		
		team = fito.team;
		
		sqd = fito.sqd;
		
		hp = fito.hp;
		speed = fito.speed;
		speedMx = fito.speedMx;
		
		wpn = fito.wpn;
		attMd = fito.attMd;
		attTimer = fito.attTimer;
		
				
		

	}

	public boolean askSqdmt() {
		if (sqd.roster.size() < 5) return true;
		return false;
	}

	public Fighter addSqdMt( Enemy mate ) {
		mate.sqd = sqd;
		mate.speed = speedMx;
		mate.speedMx = speedMx;
		//		mate.hp = 15;
		sqd.roster.add(mate);
		if (sqd.roster.size() > 1) 
			return sqd.roster.get(sqd.roster.size()-2);
		else 
			return this;
	}

	public void stun(int stunt) {
		//				System.out.println( "STUN " + this);
		stuntime = stunt;
		frozen = true;
		move = false;
	}

	public void activity(int delta) {
		fActivity(delta);
		if (frozen) {
			stuntimer += delta;
			if (stuntimer > stuntime) {
				frozen = false;
				stuntimer = 0;

//				if (Maino.server) {
//					//					System.out.println( "UNSSTUN " + this);
//					DataPok sendpl = new DataPok();
//					sendpl.type = 41;
//					sendpl.ent = this;
//					Maino.serv.broadcast(sendpl);
//				}

			}
		}


	}

	public void reload() {
		if (wpn != null)
			wpn.reload();
		//		if (!wpn.reload) {
		//			wpn.reload = true;
		//			wpn.ready = false;
		//			wpn.ammo = 0;
		//			
		//			Musor mus = new Musor(Maino.wld.plr, 1);
		//			
		//			if (Maino.client && Maino.connected) {
		//				DataPok send = new DataPok();
		//				send.type = 5;
		//				send.ent = mus;
		//				Maino.clit.sendTCP(send);
		//			}
		//			if (Maino.server)
		//				Maino.wld.addObj(mus, null, true);
		//		}

	}



	public void updato(Player plr, int type) {
		switch (type) {
		case 41:
			id = plr.id;
			
			speed = plr.speed;
			speedMx = plr.speedMx;

			attMd = plr.attMd;
			attTimer = plr.attTimer;
			
			hp = plr.hp;
			//			wpn = plr.wpn;
			//			if (plr.frozen) System.out.println( "!!!!!FREEZE!!!!! " + System.currentTimeMillis());
			frozen = plr.frozen;
			stuntime = plr.stuntime;

		case 4:
			//			if (Maino.server && plr.frozen) {
			//				
			//				System.out.println( "-----case 4 " + System.currentTimeMillis());
			//
			//			}
			//			mdir = plr.mdir;
			fdir = plr.fdir;
			x = plr.x;
			y = plr.y;
			move = plr.move;
			team = plr.team;
			deswpn = plr.deswpn;

			break;

		}
		//		if (type == 4) {
		//			x = plr.x;
		//			y = plr.y;
		//			move = plr.move;
		//			team = plr.team;
		//		}
		//		if (type == 41) {
		//			speed = plr.speed;
		//			dir = plr.dir;
		//			hp = plr.hp;
		//		}

	}



	//	public void draw(Graphics g) {
	//		if (move)
	//		switch (dir) {
	////		case 0:
	////			g.drawAnimation(left, x, y);
	////			break;
	//		case 1:
	//			g.drawAnimation(up, x, y);
	//			break;
	//		case 0:
	//		case 3:
	//		case 2:
	//			g.drawAnimation(down, x, y);
	//			break;
	//		}
	//		else g.drawImage(down.getImage(0), x, y);
	//	}

}
