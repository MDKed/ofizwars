package com.mdked.owars.units;

import com.mdked.owars.core.Maino;
import com.mdked.owars.core.World;
import com.mdked.owars.core.werld.Fraction;
import com.mdked.owars.units.strat.Facility;

import grid.GridLocation;

public class Enemy extends Fighter {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6848652018990090241L;

	public  int idleTimer, midleTimer,  stimer, stimerM, atimer, burst, mburst, cx, cy, ctimer;

	public  Entity target, ftgt;

	public Facility facTarget;

	//	public transient Squad sqd;

	public transient APath path;
	public transient GridLocation tgtLoc;
//	private  AiManager aim;
//	public  int line;

	private  boolean idle, agro;
	public  boolean follow, pfollow;
	private int flwIn = 32, flwOut = 72;

	//	public Ray rey;


	public Enemy() {}

	public Enemy(int x, int y, Fraction team) {
		xxWidth = 26;
		yyWidth = 11;
		xWidth = 12;
		yWidth = 35;

//		aim = ii;

		stimer = 0;
		stimerM = 550;
		type = 1;

		this.team = team;

		hp = 50;
		//		wpn = new Weapon((int) (Math.random()*2), id);

		this.x = x;
		this.y = y;
		speed = 70f;
		speedMx = speed;
	}

	//	public void update(Enemy up) {
	//		this.x = up.x;
	//		this.y = up.y;
	//		this.move = up.move;
	//		this.fdir = up.fdir;
	//	}

	public void refreshFtgt() {
		int oid = ftgt.id;
		ftgt = Maino.wld.getIdO(oid);
	}

	//	public void drawRay(Graphics g, GameContainer gcc) {
	//		if (rey != null)
	//		rey.drawRey( g, gcc);
	//	}

//	private boolean rayCastTotgt(Entity tegt) {
//		Ray lov = new Ray(this, tegt);
//
//		//		if (Maino.debug)
//		//			rey = lov;
//
////		if (lov.rayCast() < wpn.penetr*2) return true;
//
//		return false;
//
//	}

	public void searchTgt(int delta) {
		stimer += delta;

		if (stimer >= stimerM) {
			stimer = 0;
			stimerM = (int) (500 + 1500*Math.random());



			//			Fighter qt;
			int ld = 750;
//					wpn.distance/4;
//			ArrayList<Entity> objcs = Maino.wld.getObjects();

			for (int a = -1; a < Maino.wld.ais.size(); a ++) {
				Entity chk = a == -1 ? Maino.wld.plr : Maino.wld.ais.get(a);
				if (chk.type >= 0  &&
						chk.type <= 1 && World.getDist(this, chk) < ld) {
					Fighter ft = (Fighter) chk;
					if (ft.team != team) 
						if (World.getDist(this, ft) <= ld ) {
							target = ft;
							agro = true;
							ld = (int) World.getDist(this, ft);
						}
					//&& rayCastTotgt(ft)
				}

				if (target == null)
					newOrder(1);

			}
		}

	}

	private void nextMove() {
		tgtLoc = (GridLocation) path.path.getNextMove();
		cx = (int) x;
		cy = (int) y;
		ctimer = 0;
	}

	public void moveTo(APath to) {
		if (to == null || to.path == null) return;
		path = to;
		//		System.out.println( to );
		nextMove();
		nextMove();
	}


	/**
	 * ne tupit
	 * @param delta
	 */
	private void chekMDist(int delta) {
		//		if (follow) return;
		ctimer += delta;
		if (ctimer > 3000) {

			ctimer = 0;
			//			System.out.println(World.getDist(cx, x, cy, y));w
			if (World.getDist(cx, x, cy, y) < 15) {
				if (!follow)
					newOrder(0);
				else if (pfollow)
					newOrder(3);
			}
			
			if (pfollow)
				newOrder(3);

			cx = (int) x;
			cy = (int) y;
		}
	}
	//
	//	private int[] getTGTXY() {
	//		int[] ret = {path.otnX + tgtLoc.getX()*16 - 2, path.otnY + tgtLoc.getY()*16 - 2};
	//		return ;
	//	}

	private synchronized void gobot() {
		int razbnah = 8;

		if (tgtLoc == null) {
			if (!follow)
				newOrder(0);
			else 
				newOrder(4);
			//			System.out.println("tlo1");
			return;
		}

		move = true;

		if (!follow) {

			if (x >= path.otnX + tgtLoc.getX()*World.kvx - razbnah && x <= path.otnX + tgtLoc.getX()*World.kvx + razbnah &&
					y >= path.otnY + tgtLoc.getY()*World.kvx - razbnah && y <= path.otnY + tgtLoc.getY()*World.kvx + razbnah) 
				nextMove();


			if (tgtLoc == null) {
				//				System.out.println("tlo2");
				newOrder(0);
				return;
			} 

			dx = (int) (x -(path.otnX +tgtLoc.getX()*World.kvx));
			dy = (int) (y -(path.otnY + tgtLoc.getY()*World.kvx));
		} 
		else {
			if (pfollow) {
				
				if (path == null) return;
				
//				System.out.println(tgtLoc.getX());
				if (x >= path.otnX 
						+ tgtLoc.getX()*World.kvx - 
						razbnah && x <= path.otnX + tgtLoc.getX()*World.kvx + razbnah &&
						y >= path.otnY + tgtLoc.getY()*World.kvx - razbnah && y <= path.otnY + tgtLoc.getY()*World.kvx + razbnah) 
					nextMove();

				if (tgtLoc == null) {
					//													System.out.println("tpfofof2");
					//					checkFollow();
					newOrder(3);
					return;
				} 

//				System.out.println(tgtLoc.getX());
				
				dx = (int) (x - (path.otnX + 
						tgtLoc.getX()
						*World.kvx));
				dy = (int) (y - (path.otnY + tgtLoc.getY()*World.kvx));
				
				if (ftgt == null)
					newOrder(2);
				
				if (World.getDist(ftgt, this) < flwOut) 
					pfollow = false;


			} else {
				if (ftgt == null)
				{
					follow = false;
					newOrder(1);
				}
				//			int dist = (int) World.getDist(ftgt, this);
				//			if (dist)
				dx = (int) (x - ftgt.x );
				dy = (int) (y - ftgt.y );
			}
		}

		setMDir();
		mdir += Math.toRadians(-5+ 10*Math.random());
		//				System.out.println("go bot! " + " bto xy: " + x + "x" + y + " " + mdir + " tgtx " + tgtLoc.getX()*16 + " tgty " +tgtLoc.getY()*16);
	}

	public void followTarget(Fighter tgt) {
		follow = true;
		ftgt = tgt;
	}

	public boolean checkFollow() {
//				if (follow && !pfollow)	System.out.println(" check follow " + World.getDist(this, ftgt));
		if (!follow)
			return true;
		
		else if (!pfollow &&  World.getDist(this, ftgt) > flwOut ) {
			//			System.out.println(" >50");
			newOrder(3);
		}
		else if (!pfollow &&  World.getDist(this, ftgt) < flwIn ) {
			move = false;
			return false;

		}  
		return true;


	}

/**
 * 0 - idle
 * 1 - deagro
 * 2 - patrol
 * 3 - follow search
 * 4 - idle 400
 * 5 - take path
 * 6 - idle 1000
 */
	private void newOrder(int phs) {

		switch (phs) {
		case 0:
			//			System.out.println(" order inc " + phs);
			idle = true;
			idleTimer = 0;
			midleTimer = (int) (2000 + (15000 * Math.random()));
			break;

		case 1:
			agro = false;
			break;

		case 2:
			idle = false;
			if (!follow) {

				if (facTarget == null)
					getRandomPoint(6);
				else {
					Entity wp = facTarget.getRandomWaypoint();
					if (wp != null) {
//						APath wer =
								Maino.aim.getPathto(this,(float) (wp.x+ (Math.random()*30f)), (float) (wp.y+ (Math.random()*30f)));
								newOrder(6);
//						moveTo(wer);
					} else 
						getRandomPoint(6);
				}

				break;
			}
			
		case 3:
			// daleko ot baty
//						System.out.println(" case 3 " + this);
			if (World.getDist(x, ftgt.x, y, ftgt.y) > 64) {
				pfollow = true;
				//				if (ftgt.type == 0)
//				float[] xxa = aim.getOtnXY(x, y);
				//					System.out.println("-case follow " + id + " xy " + xxa[0]/16 + "x" + xxa[1]/16);
//				moveTo(
						Maino.aim.getPathto(this, ftgt.x, ftgt.y);
						newOrder(6);
//						);
			}
			break;

		case 4:
			idle = true;
			idleTimer = 0;
			midleTimer = 400;
			break;
			
		case 5:
			//pfind give path
			idle = false;
			moveTo(path);
			break;
		case 6:
			//idle for path
			idle = true;
			idleTimer = 0;
			midleTimer = 1000;
			break;
		}

	}
	
	public synchronized void givePath(APath pathe) {
		path = pathe;
		newOrder(5);
	}

	public void getRandomPoint(int dist) {
		//		GridLocation start =  new GridLocation((int) x/16,(int)   y/16, false);
		//				aim.getNotWalledPoint((int) x,(int) y, false);
		//					 new GridLocation((int) tgtf.x/16,(int)  tgtf.y/16, true);
		int rx = (int) (dist/2f - (Math.random() * dist)),
				ry = (int) (dist/2f - (Math.random() * dist));
		//		System.out.println("ke " + rx + "x" + ry);

		int xx = (int) (x + (rx*World.kvx)), yy = (int) (y + (ry*World.kvx));
		//		GridLocation end = aim.getNotWalledPoint(xx, yy, true);
		//
		//		GridPathfinding pathfinding = new GridPathfinding();


//		APath pathe = 
		Maino.aim.getPathto(this, xx, yy);
//		if (pathe == null || pathe.path == null) {
			newOrder(0);
//			return;
//		}
//		moveTo(pathe);
	}

	public synchronized void think(int delta) {
		
//		Maino.getDelta(false);
		//		if (World.getDist(this, ftgt) > 50) {
		//			System.out.println("dist - " + World.getDist(this, ftgt) +
		//					" f- " + follow + " pf - " + pfollow  + " idl - " +
		//					idle + " idletiem " + idleTimer + "\\" + midleTimer);
		//		}

//				System.out.println(id + " wpnid " + wpn.owner);
		fActivity(delta);

		wpn.reload(delta);
		move = false;
//		Maino.getDelta();
		searchTgt(delta);
//		Maino.getDelta(false);
		//		if (follow && !pfollow)
		//			System.out.println(" wat " + World.getDist(this, ftgt) + " tgt = "+ ftgt);
		//		&& !agro
//		Maino.getDelta();
		//FIXME 160000 nanos
		if (!idle  ) {
			gobot();
			if (checkFollow())
				movebyrot(delta);
			chekMDist(delta);
		} 
		
//		Maino.getDelta(false);
		if (idle) {
			idleTimer += delta;
			//						if (follow && !pfollow)
			//						System.out.println(" check follow " + World.getDist(this, ftgt));
		}


		if (agro && target != null) 
			fightLogic(delta);
		else newOrder(1);

		if (idleTimer >= midleTimer && idle) {
			//			System.out.println("idol");
			newOrder(2);
		}
//		Maino.getDelta(false);
		}

	private void fightLogic(int delta) {

		if (fire(delta, (int) target.x+ target.xxWidth+4, (int)  target.y+32)) 
			burst++;
		fdir = (float) (Math.atan2((y-target.y), (x-target.x)) );

		if (burst >= 3) {
			burst = 0;
			mburst = (int) (3 + (3*Math.random()));
			newOrder(1);

		}
	}



	//	public void update(EnDPak inc) {
	//		x = inc.x;
	//		y = inc.y;
	//		move = inc.move;
	//		fdir = inc.fdir;
	//	}

}
