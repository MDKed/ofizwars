package com.mdked.owars.units.strat;

import java.util.ArrayList;

import javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec;

import com.mdked.owars.core.Maino;
import com.mdked.owars.core.World;
import com.mdked.owars.core.werld.Fraction;
import com.mdked.owars.units.Entity;
import com.mdked.owars.units.Respawn;
/**
 * 100 = city;
 * 200 = base;
 * 201 = mobile base(respawn);
 * 
 * +2000 = constructions
 * 2201 - mbase building
 * @param type
 */
public class Facility extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6404671129716396158L;

	public int soldiers, soldiersMax, garizon;
	public Fraction owner;
	public ArrayList<Respawn> spawns;
	public ArrayList<Entity> waypoints;
	public int size, bTimer, bEndTimer, bResType;
	public boolean built;


	public Facility(int type) {
		this.type = type;

		waypoints = new ArrayList<>();
		spawns = new ArrayList<Respawn>();
	}

	public Facility(int x, int y, int type, Fraction rndNPF, int size) {
		this(type);
		this.x = x;
		this.y = y;

		this.size = size;
		owner = rndNPF;
		setSize(size);

		switch (type) {
		case 2201:
			xWidth = 64*3;
			yWidth = 64*3;
			bEndTimer = 5000;
			break;
		}
	}

	public void setBuild() {
		built = true;
	}

	public void setSize(int size) {
		soldiersMax = size;
		garizon = size*3;
	}

	public void soldierLost() {
		if (type == 201) return;
		//				System.out.println(" Garizon = " + garizon);
		soldiers--;
		garizon--;



	}

	public void plrSqdSpwn() {
		if (type == 201) {
			garizon--;
			if (garizon <= 0) {
				Maino.wld.remCObj(this);
				Maino.wld.remObj(this);
			}
		}
	}

	public Respawn getRandSpawn() {
		return  spawns.get((int) (Math.random()*spawns.size()));
	}

	public void initSoldSpawn() {
		while (garizon > 0 && soldiers < soldiersMax) {
			soldiers++;
			Maino.aim.addBot(owner, 
					getRandSpawn(), 
					this);
		}
	}

	public void spawnSoldier() {
		if (garizon > 0 && soldiers < soldiersMax) {
			soldiers++;
			Maino.aim.addBot(owner, getRandSpawn(), this);
			//			if (type == 201) 
			garizon--;
		}

	}

	public Entity getRandomWaypoint() {
		if (waypoints.size() > 0)
			return waypoints.get((int) (waypoints.size()*Math.random()));

		return null;
	}

	public void addWaypoint(float x, float y) {
		waypoints.add(new Entity(x,y));
	}

	public void reOwn() {
		float distzah = soldiersMax/4f * 150f;
		System.out.println("reowning " + distzah);

		if (World.getDist(this, Maino.wld.plr) < distzah)
		{

			System.out.print(" Fac - " + this + " last owner - " + owner.name + " new owner ");
			owner.facs.remove(this);
			owner = Maino.wld.plr.team;
			owner.facs.add(this);
			System.out.println(owner.name);
			setSize(size);
			return;
		}

		for (int a = 0; a < Maino.wld.ais.size(); a++)
			if (World.getDist(this, Maino.wld.ais.get(a)) < distzah) {

				System.out.print(" Fac - " + this + " last owner - " + owner.name + " new owner ");
				owner.facs.remove(this);
				owner = Maino.wld.ais.get(a).team;
				owner.facs.add(this);
				System.out.println(owner.name);

				setSize(size);

				return;
			}
	}

	public void checkFac(int delta) {
		if ( soldiers <= 0 && garizon <= 0) {
			reOwn();

		}

		//NALOGI
		if (type == 100) {
			//			System.out.println("Nalog");
			owner.money += 	(size/60f) * (delta/1000f);
		}

		if (type >= 2000) {
			bTimer += delta;
			if (bTimer >= bEndTimer) {
				transformFac();
			}
		}
	}

	private void transformFac() {
		switch (type) {
		case 2201:
			//mbase
			built = true;
			type = 201;
			addSpawn((int) (x+32 + (World.kvx*2)), (int) (y+32 + (World.kvx*3)));
			garizon *= 10;
			break;
		}
	}

	public void addSpawn(int x, int y) {
		spawns.add(new Respawn(x, y, this));
	}

	public void deltFar() {
		soldiers--;
		garizon++;

	}

}
